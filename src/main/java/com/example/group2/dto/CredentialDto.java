package com.example.group2.dto;

import com.example.group2.entity.Credential;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CredentialDto {

    private String text;

    private Credential.Type type;

    private LocalDateTime createdAt;

    private String version;
}
