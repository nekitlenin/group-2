package com.example.group2.controller;

import com.example.group2.dto.FileResponseDto;
import com.example.group2.entity.File;
import com.example.group2.mapper.FileMapper;
import com.example.group2.service.FileService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 17.10.2023 15:54 |
 * Created with IntelliJ IDEA
 */
@WebMvcTest(FileController.class)
public class FileControllerTest {
    private static final String URL = "/files";

    private static final Long EXIST_ID = 1L;

    private static final Long NOT_EXIST_ID = 2L;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private FileService fileService;

    @MockBean
    private FileMapper fileMapper;

    @BeforeEach
    public void setUp() {
        FileResponseDto fileResponseDto = FileResponseDto.builder()
                .name("Test text")
                .build();
        File file = File.builder()
                .id(1L)
                .name("Test text")
                .build();
        when(fileService.save(any())).thenReturn(Optional.of(file));
        when(fileService.getAll()).thenReturn(List.of(new File(), new File()));
        when(fileService.getById(anyLong())).thenReturn(Optional.of(file));
        when(fileService.deleteSoft(anyLong())).thenReturn(Optional.of(file));

        when(fileMapper.entityToResponseDto(file)).thenReturn(fileResponseDto);
        when(fileMapper.dtoToEntity(any())).thenReturn(file);
        when(fileMapper.updateEntityFromDto(anyLong(), any(), any())).thenReturn(file);
    }

    @Test
    public void test_create() throws Exception {
        mockMvc.perform(post(URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"name\": \"Test text\"}"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value("Test text"));
    }

    @Test
    public void test_update() throws Exception {
        mockMvc.perform(put(URL + "/" + EXIST_ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"name\": \"Test text\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Test text"));
    }

    @Test
    public void test_update_notFound() throws Exception {
        when(fileService.getById(NOT_EXIST_ID)).thenReturn(Optional.empty());

        mockMvc.perform(put(URL + "/" + NOT_EXIST_ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"name\": \"Test text\"}"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void test_getAll() throws Exception {
        mockMvc.perform(get(URL))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(2));
    }

    @Test
    public void test_getAll_notFound() throws Exception {
        when(fileService.getAll()).thenReturn(List.of());

        mockMvc.perform(get(URL))
                .andExpect(status().isNotFound());
    }

    @Test
    public void test_getById() throws Exception {
        mockMvc.perform(get(URL + "/" + EXIST_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Test text"));
    }

    @Test
    public void test_getById_notFound() throws Exception {
        when(fileService.getById(NOT_EXIST_ID)).thenReturn(Optional.empty());

        mockMvc.perform(get(URL + "/" + NOT_EXIST_ID))
                .andExpect(status().isNotFound());
    }

    @Test
    public void test_delete() throws Exception {
        mockMvc.perform(delete(URL + "/" + EXIST_ID))
                .andExpect(status().isOk());

        verify(fileService, times(1)).deleteSoft(EXIST_ID);
    }

    @Test
    public void test_delete_notFound() throws Exception {
        when(fileService.deleteSoft(NOT_EXIST_ID)).thenReturn(Optional.empty());

        mockMvc.perform(delete(URL + "/" + NOT_EXIST_ID))
                .andExpect(status().isNotFound());

        verify(fileService, times(1)).deleteSoft(NOT_EXIST_ID);
    }
}
