package com.example.group2.controller;

import com.example.group2.dto.FieldResponseDto;
import com.example.group2.entity.Field;
import com.example.group2.mapper.FieldMapper;
import com.example.group2.service.FieldService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 17.10.2023 15:53 |
 * Created with IntelliJ IDEA
 */
@WebMvcTest(FieldController.class)
public class FieldControllerTest {
    private static final String URL = "/fields";

    private static final Long EXIST_ID = 1L;

    private static final Long NOT_EXIST_ID = 2L;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private FieldService fieldService;

    @MockBean
    private FieldMapper fieldMapper;

    @BeforeEach
    public void setUp() {
        FieldResponseDto fieldResponseDto = FieldResponseDto.builder()
                .name("Test text")
                .build();
        Field field = Field.builder()
                .id(1L)
                .name("Test text")
                .build();
        when(fieldService.save(any())).thenReturn(Optional.of(field));
        when(fieldService.getAll()).thenReturn(List.of(new Field(), new Field()));
        when(fieldService.getById(anyLong())).thenReturn(Optional.of(field));
        when(fieldService.deleteSoft(anyLong())).thenReturn(Optional.of(field));

        when(fieldMapper.entityToResponseDto(field)).thenReturn(fieldResponseDto);
        when(fieldMapper.dtoToEntity(any())).thenReturn(field);
        when(fieldMapper.updateEntityFromDto(anyLong(), any(), any())).thenReturn(field);
    }

    @Test
    public void test_createField() throws Exception {
        mockMvc.perform(post(URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"name\": \"Test text\"}"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value("Test text"));
    }

    @Test
    public void test_updateField() throws Exception {
        mockMvc.perform(put(URL + "/" + EXIST_ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"name\": \"Test text\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Test text"));
    }

    @Test
    public void test_updateField_notFound() throws Exception {
        when(fieldService.getById(NOT_EXIST_ID)).thenReturn(Optional.empty());

        mockMvc.perform(put(URL + "/" + NOT_EXIST_ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"name\": \"Test text\"}"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void test_getAllFields() throws Exception {
        mockMvc.perform(get(URL))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(2));
    }

    @Test
    public void test_getAllFields_notFound() throws Exception {
        when(fieldService.getAll()).thenReturn(List.of());

        mockMvc.perform(get(URL))
                .andExpect(status().isNotFound());
    }

    @Test
    public void test_getFieldById() throws Exception {
        mockMvc.perform(get(URL + "/" + EXIST_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Test text"));
    }

    @Test
    public void test_getFieldById_notFound() throws Exception {
        when(fieldService.getById(NOT_EXIST_ID)).thenReturn(Optional.empty());

        mockMvc.perform(get(URL + "/" + NOT_EXIST_ID))
                .andExpect(status().isNotFound());
    }

    @Test
    public void test_deleteField() throws Exception {
        mockMvc.perform(delete(URL + "/" + EXIST_ID))
                .andExpect(status().isOk());

        verify(fieldService, times(1)).deleteSoft(EXIST_ID);
    }

    @Test
    public void test_deleteField_notFound() throws Exception {
        when(fieldService.deleteSoft(NOT_EXIST_ID)).thenReturn(Optional.empty());

        mockMvc.perform(delete(URL + "/" + NOT_EXIST_ID))
                .andExpect(status().isNotFound());

        verify(fieldService, times(1)).deleteSoft(NOT_EXIST_ID);
    }
}
