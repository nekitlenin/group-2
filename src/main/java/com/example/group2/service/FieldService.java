package com.example.group2.service;

import com.example.group2.entity.Field;
import com.example.group2.repository.FieldRepository;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class FieldService extends AbstractService<Field, FieldRepository> {

    private final FieldRepository fieldRepository;

    public FieldService(FieldRepository fieldRepository) {
        super(fieldRepository);
        this.fieldRepository = fieldRepository;
    }
}
