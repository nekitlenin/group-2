package com.example.group2.entity;


import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "documents")
public class Document extends SoftDeleteEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "content")
    private String content;

    @Setter(AccessLevel.NONE)
    @Column(name = "date_create")
    private LocalDateTime createdAt;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "type")
    private Type type;

    public enum Type {
        CONTACT, /* Договор */
        AGREEMENT, /* Соглашение */
        APPLICATION, /* Заявка */
        ACT, /* Акт */
        STATEMENT /* Справка */
    }

    @ManyToOne
    @JoinColumn(name = "counterparty_id")
    public Counterparty counterparty;

    @ManyToOne
    @JoinColumn(name = "template_id")
    private Template template;

    @ManyToMany
    private List<File> files;

    @OneToMany
    @ToString.Exclude
    private List<Field> fields;

    @PrePersist
    private void onCreate() {
        this.createdAt = LocalDateTime.now();
    }
}
