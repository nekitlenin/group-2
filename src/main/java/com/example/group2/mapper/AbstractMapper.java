package com.example.group2.mapper;

import org.mapstruct.MappingTarget;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 12.10.2023 23:56 |
 * Created with IntelliJ IDEA
 */
public interface AbstractMapper<D, E, R> {
    E dtoToEntity(D dto);

    E responseDtoToEntity(R responseDto);

    R entityToResponseDto(E entity);

    E updateEntityFromDto(Long id, @MappingTarget E entity, D dto);
}
