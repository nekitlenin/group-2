package com.example.group2.mapper;

import com.example.group2.dto.CredentialDto;
import com.example.group2.dto.CredentialResponseDto;
import com.example.group2.entity.Credential;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface CredentialMapper {
    Credential dtoToEntity(CredentialDto dto);

    Credential responseDtoToEntity(CredentialResponseDto responseDto);

    CredentialResponseDto entityToResponseDto(Credential entity);

    Credential updateEntityFromDto(Long id, @MappingTarget Credential entity, CredentialDto dto);
}
