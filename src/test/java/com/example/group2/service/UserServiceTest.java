package com.example.group2.service;

import com.example.group2.entity.User;
import com.example.group2.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 28.09.2023 22:27 |
 * Created with IntelliJ IDEA
 */
@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @InjectMocks
    private UserService userService;

    @Mock
    private UserRepository userRepository;

    @Test
    void save() {
        User userToSave = new User();
        userToSave.setId(1L);

        Mockito.when(userRepository.save(any(User.class))).thenReturn(userToSave);
        Optional<User> saveUser = userService.save(userToSave);

        saveUser.ifPresent(user -> assertEquals(1L, user.getId()));
    }

    @Test
    void getById() {
        User userToFound = new User();
        userToFound.setId(1L);
        userToFound.setFirstName("Test File");

        Mockito.when(userRepository.findByIdAndIsDeletedFalse(1L)).thenReturn(Optional.of(userToFound));
        Optional<User> foundUser = userService.getById(1L);

        foundUser.ifPresent(user -> {
            assertEquals(1L, user.getId());
            assertEquals("Test File", user.getFirstName());
        });
    }

    @Test
    void getAll() {
        List<User> users = new ArrayList<>();
        users.add(new User());
        users.add(new User());
        users.add(new User());

        Mockito.when(userRepository.findAllByIsDeletedFalse()).thenReturn(users);
        List<User> allUsers = userService.getAll();

        assertEquals(3, allUsers.size());
    }

    @Test
    void delete() {
        User user = new User();
        user.setId(1L);
        user.setIsDeleted(false);

        Mockito.when(userRepository.findByIdAndIsDeletedFalse(1L)).thenReturn(Optional.of(user));
        userService.deleteSoft(1L);

        assertTrue(user.getIsDeleted());
    }
}