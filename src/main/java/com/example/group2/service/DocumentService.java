package com.example.group2.service;


import com.example.group2.entity.Document;
import com.example.group2.entity.Template;
import com.example.group2.repository.DocumentRepository;
import com.example.group2.repository.TemplateRepository;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Transactional
public class DocumentService extends AbstractService<Document, DocumentRepository> {

    private final DocumentRepository documentRepository;

    private final TemplateRepository templateRepository;

    public DocumentService(DocumentRepository documentRepository, TemplateRepository templateRepository) {
        super(documentRepository);
        this.documentRepository = documentRepository;
        this.templateRepository = templateRepository;
    }

    public Optional<Document> createFromTemplate(Long templateId, Document document) {
        Template template = templateRepository.findByIdAndIsDeletedFalse(templateId).orElse(null);

        if (template == null)
            return Optional.empty();
        document.setName(template.getName());
        document.setContent(template.getContent());
        document.setTemplate(template);

        return Optional.of(documentRepository.save(document));
    }
}
