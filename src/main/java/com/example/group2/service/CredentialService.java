package com.example.group2.service;

import com.example.group2.entity.Credential;
import com.example.group2.repository.CredentialRepository;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 22.09.2023 14:17 |
 * Created with IntelliJ IDEA
 */
@Service
@Transactional
public class CredentialService extends AbstractService<Credential, CredentialRepository> {

    private final CredentialRepository credentialRepository;

    public CredentialService(CredentialRepository credentialRepository) {
        super(credentialRepository);
        this.credentialRepository = credentialRepository;
    }
}
