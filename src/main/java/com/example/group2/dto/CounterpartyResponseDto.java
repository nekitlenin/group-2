package com.example.group2.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 24.10.2023 10:18 |
 * Created with IntelliJ IDEA
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CounterpartyResponseDto {

    private Long id;

    private String firstName;

    private String lastName;

    private String middleName;

    private String phoneNumber;

    private String telegramName;

    private String email;

    private String country;

    private List<Long> credentialIds;

    private List<Long> commentIds;
}
