package com.example.group2.service;

import com.example.group2.entity.Comment;
import com.example.group2.repository.CommentRepository;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class CommentService extends AbstractService<Comment, CommentRepository> {

    private final CommentRepository commentRepository;

    public CommentService(CommentRepository commentRepository) {
        super(commentRepository);
        this.commentRepository = commentRepository;
    }
}
