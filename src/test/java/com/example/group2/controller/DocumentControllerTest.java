package com.example.group2.controller;

import com.example.group2.dto.DocumentResponseDto;
import com.example.group2.entity.Document;
import com.example.group2.mapper.DocumentMapper;
import com.example.group2.service.DocumentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 17.10.2023 15:53 |
 * Created with IntelliJ IDEA
 */
@WebMvcTest(DocumentController.class)
public class DocumentControllerTest {
    private static final String DOCUMENTS = "/documents";
    private static final String TEMPLATE = "template";
    private static final Long EXISTS_TEMPLATE_ID = 1L;
    private static final Long NOT_EXISTS_TEMPLATE_ID = 2L;
    private static final Long EXISTS_DOCUMENT_ID = 1L;
    private static final Long NOT_EXIST_DOCUMENT_ID = 2L;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DocumentService documentService;

    @MockBean
    private DocumentMapper documentMapper;

    private DocumentResponseDto documentResponseDto;

    private Document document;

    @BeforeEach
    public void setUp() {
        documentResponseDto = DocumentResponseDto.builder()
                .name("Test text")
                .createdAt(LocalDateTime.now())
                .counterpartyId(1L)
                .templateId(1L)
                .build();
        document = Document.builder()
                .id(1L)
                .name("Test text")
                .createdAt(LocalDateTime.now())
                .build();

        when(documentService.save(any())).thenReturn(Optional.of(document));
        when(documentService.createFromTemplate(anyLong(), any())).thenReturn(Optional.of(document));
        when(documentService.getAll()).thenReturn(List.of(new Document(), new Document()));
        when(documentService.getById(anyLong())).thenReturn(Optional.of(document));
        when(documentService.deleteSoft(anyLong())).thenReturn(Optional.of(document));

        when(documentMapper.entityToResponseDto(document)).thenReturn(documentResponseDto);
        when(documentMapper.dtoToEntity(any())).thenReturn(document);
        when(documentMapper.updateEntityFromDto(anyLong(), any(), any())).thenReturn(document);
    }

    @Test
    void test_updateExistingDocumentFromTemplate() throws Exception {
        mockMvc.perform(put(DOCUMENTS + "/" + EXISTS_DOCUMENT_ID + "/" + TEMPLATE)
                        .content("{}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Test text"));
    }

    @Test
    void testUpdateExistingDocumentFromTemplate() throws Exception {
        mockMvc.perform(put(DOCUMENTS + "/" +
                        EXISTS_DOCUMENT_ID + "/" +
                        TEMPLATE + "/" +
                        EXISTS_TEMPLATE_ID)
                        .content("{}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Test text"));
    }

    @Test
    void test_createNewDocumentFromTemplate() throws Exception {
        mockMvc.perform(post(DOCUMENTS + "/" + TEMPLATE + "/" + EXISTS_TEMPLATE_ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"name\": \"Test text\"}"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value("Test text"));
    }

    @Test
    public void test_create() throws Exception {
        mockMvc.perform(post(DOCUMENTS)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"name\": \"Test text\"}"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value("Test text"));
    }

    @Test
    public void test_update() throws Exception {
        mockMvc.perform(put(DOCUMENTS + "/" + EXISTS_DOCUMENT_ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"name\": \"Test text\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Test text"));
    }

    @Test
    public void test_update_notFound() throws Exception {
        when(documentService.getById(NOT_EXIST_DOCUMENT_ID)).thenReturn(Optional.empty());

        mockMvc.perform(put(DOCUMENTS + "/" + NOT_EXIST_DOCUMENT_ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"name\": \"Test text\"}"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void test_getAll() throws Exception {
        mockMvc.perform(get(DOCUMENTS))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(2));
    }

    @Test
    public void test_getAll_notFound() throws Exception {
        when(documentService.getAll()).thenReturn(List.of());

        mockMvc.perform(get(DOCUMENTS))
                .andExpect(status().isNotFound());
    }

    @Test
    public void test_getById() throws Exception {
        mockMvc.perform(get(DOCUMENTS + "/" + EXISTS_DOCUMENT_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Test text"));
    }

    @Test
    public void test_getById_notFound() throws Exception {
        when(documentService.getById(NOT_EXIST_DOCUMENT_ID)).thenReturn(Optional.empty());

        mockMvc.perform(get(DOCUMENTS + "/" + NOT_EXIST_DOCUMENT_ID))
                .andExpect(status().isNotFound());
    }

    @Test
    public void test_delete() throws Exception {
        mockMvc.perform(delete(DOCUMENTS + "/" + EXISTS_DOCUMENT_ID))
                .andExpect(status().isOk());

        verify(documentService, times(1)).deleteSoft(EXISTS_DOCUMENT_ID);
    }

    @Test
    public void test_delete_notFound() throws Exception {
        when(documentService.deleteSoft(NOT_EXIST_DOCUMENT_ID)).thenReturn(Optional.empty());

        mockMvc.perform(delete(DOCUMENTS + "/" + NOT_EXIST_DOCUMENT_ID))
                .andExpect(status().isNotFound());

        verify(documentService, times(1)).deleteSoft(NOT_EXIST_DOCUMENT_ID);
    }
}
