package com.example.group2.service;

import com.example.group2.entity.Credential;
import com.example.group2.repository.CredentialRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 28.09.2023 22:26 |
 * Created with IntelliJ IDEA
 */
@ExtendWith(MockitoExtension.class)
class CredentialServiceTest {

    @InjectMocks
    private CredentialService credentialService;

    @Mock
    private CredentialRepository credentialRepository;

    @Test
    public void testSaveCredential() {
        Credential credentialToSave = new Credential();
        credentialToSave.setId(1L);
        credentialToSave.setIsDeleted(false);

        Mockito.when(credentialRepository.save(any(Credential.class))).thenReturn(credentialToSave);
        Optional<Credential> saveCredential = credentialService.save(credentialToSave);

        saveCredential.ifPresent(credential -> assertEquals(1L, credential.getId()));
    }

    @Test
    public void testGetCredentialById() {
        Credential credentialToFound = new Credential();
        credentialToFound.setId(1L);
        credentialToFound.setText("Test Credential");

        Mockito.when(credentialRepository.findByIdAndIsDeletedFalse(1L)).thenReturn(Optional.of(credentialToFound));
        Optional<Credential> foundCredential = credentialService.getById(1L);

        foundCredential.ifPresent(counterparty -> {
            assertEquals(1L, counterparty.getId());
            assertEquals("Test Credential", counterparty.getText());
        });
    }

    @Test
    public void testGetAllCredentials() {
        List<Credential> credentials = new ArrayList<>();
        credentials.add(new Credential());
        credentials.add(new Credential());
        credentials.add(new Credential());

        Mockito.when(credentialRepository.findAllByIsDeletedFalse()).thenReturn(credentials);
        List<Credential> allCredentials = credentialService.getAll();

        assertEquals(3, allCredentials.size());
    }

    @Test
    public void testDeleteCounterparty() {
        Credential credential = new Credential();
        credential.setId(1L);
        credential.setIsDeleted(false);

        Mockito.when(credentialRepository.findByIdAndIsDeletedFalse(1L)).thenReturn(Optional.of(credential));
        credentialService.deleteSoft(1L);

        assertTrue(credential.getIsDeleted());
    }
}