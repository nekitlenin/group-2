package com.example.group2.repository;

import com.example.group2.entity.Field;

public interface FieldRepository extends AbstractRepository<Field> {

}

