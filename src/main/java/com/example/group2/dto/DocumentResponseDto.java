package com.example.group2.dto;

import com.example.group2.entity.Document;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 23.10.2023 04:32 |
 * Created with IntelliJ IDEA
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DocumentResponseDto {

    private Long id;

    private String name;

    private String content;

    private Document.Type type;

    private LocalDateTime createdAt;

    private Long counterpartyId;

    private Long templateId;

    private List<Long> fileIds;

    private List<Long> fieldIds;
}
