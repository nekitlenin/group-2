package com.example.group2.controller;

import com.example.group2.dto.TemplateDto;
import com.example.group2.dto.TemplateResponseDto;
import com.example.group2.mapper.TemplateMapper;
import com.example.group2.service.TemplateService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 13.10.2023 12:26 |
 * Created with IntelliJ IDEA
 * <p>
 * Контроллер для работы с шаблонами.
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/templates")
public class TemplateController {

    private final TemplateService templateService;
    private final TemplateMapper templateMapper;

    /**
     * Создание нового шаблона.
     *
     * @param dto данные шаблона
     * @return созданный шаблон
     */
    @PostMapping
    public ResponseEntity<TemplateResponseDto> create(@RequestBody TemplateDto dto) {
        log.info("Creating a new template: {}", dto);
        TemplateResponseDto templateResponseDto = templateService.save(templateMapper.dtoToEntity(dto))
                .map(templateMapper::entityToResponseDto)
                .orElse(null);
        log.info("New template created successfully.");
        return new ResponseEntity<>(templateResponseDto, HttpStatus.CREATED);
    }

    /**
     * Обновление шаблона.
     *
     * @param id  идентификатор шаблона
     * @param dto новые данные шаблона
     * @return обновленный шаблон
     */
    @PutMapping(value = "/{id}")
    public ResponseEntity<TemplateResponseDto> update(@PathVariable(name = "id") Long id,
                                                      @RequestBody TemplateDto dto) {
        log.info("Updating template with ID {}: {}", id, dto);
        TemplateResponseDto templateResponseDto = templateService.getById(id)
                .map(templateMapper::entityToResponseDto)
                .orElse(null);
        if (templateResponseDto == null) {
            log.warn("Template with ID {} not found. Update failed.", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        templateResponseDto = templateService.save(templateMapper.updateEntityFromDto
                        (id, templateMapper.responseDtoToEntity(templateResponseDto), dto))
                .map(templateMapper::entityToResponseDto)
                .orElse(null);
        log.info("Template with ID {} updated successfully.", id);
        return new ResponseEntity<>(templateResponseDto, HttpStatus.OK);
    }

    /**
     * Получение всех шаблонов.
     *
     * @return список шаблонов
     */
    @GetMapping
    public ResponseEntity<List<TemplateResponseDto>> getAll() {
        log.info("Get all templates");
        List<TemplateResponseDto> dtos = templateService.getAll().stream()
                .map(templateMapper::entityToResponseDto)
                .collect(Collectors.toList());
        if (!dtos.isEmpty()) {
            log.info("Get {} templates.", dtos.size());
            return new ResponseEntity<>(dtos, HttpStatus.OK);
        } else {
            log.warn("No templates found.");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Получение шаблона по идентификатору.
     *
     * @param id идентификатор шаблона
     * @return найденный шаблон
     */
    @GetMapping(value = "/{id}")
    public ResponseEntity<TemplateResponseDto> get(@PathVariable(name = "id") Long id) {
        log.info("Get template with ID {}.", id);
        TemplateResponseDto dto = templateService.getById(id)
                .map(templateMapper::entityToResponseDto)
                .orElse(null);
        if (dto != null) {
            log.info("Get template with ID {} successfully.", id);
            return new ResponseEntity<>(dto, HttpStatus.OK);
        } else {
            log.warn("Template with ID {} not found.", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Удаление шаблона.
     *
     * @param id идентификатор шаблона
     * @return удалённый шаблон
     */
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<TemplateResponseDto> delete(@PathVariable(name = "id") Long id) {
        log.info("Deleting comment with ID {}.", id);
        TemplateResponseDto dto = templateService.deleteSoft(id)
                .map(templateMapper::entityToResponseDto)
                .orElse(null);
        if (dto != null) {
            log.info("Template with ID {} deleted successfully.", id);
            return new ResponseEntity<>(dto, HttpStatus.OK);
        } else {
            log.warn("Template with ID {} not found. Deletion failed.", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
