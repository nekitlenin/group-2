package com.example.group2.service;

import com.example.group2.entity.Counterparty;
import com.example.group2.repository.CounterpartyRepository;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 23.09.2023 14:34 |
 * Created with IntelliJ IDEA
 */
@Service
@Transactional
public class CounterpartyService extends AbstractService<Counterparty, CounterpartyRepository> {

    private final CounterpartyRepository counterpartyRepository;

    public CounterpartyService(CounterpartyRepository counterpartyRepository) {
        super(counterpartyRepository);
        this.counterpartyRepository = counterpartyRepository;
    }
}
