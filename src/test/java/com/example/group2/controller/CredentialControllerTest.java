package com.example.group2.controller;

import com.example.group2.dto.CredentialResponseDto;
import com.example.group2.entity.Credential;
import com.example.group2.mapper.CredentialMapper;
import com.example.group2.service.CredentialService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 17.10.2023 15:53 |
 * Created with IntelliJ IDEA
 */
@WebMvcTest(CredentialController.class)
public class CredentialControllerTest {
    private static final String URL = "/credentials";

    private static final Long EXIST_ID = 1L;

    private static final Long NOT_EXIST_ID = 2L;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CredentialService credentialServices;

    @MockBean
    private CredentialMapper credentialMapper;

    @BeforeEach
    public void setUp() {
        CredentialResponseDto credentialResponseDto = CredentialResponseDto.builder()
                .text("Test text")
                .createdAt(LocalDateTime.now())
                .build();
        Credential credential = Credential.builder()
                .id(1L)
                .text("Test text")
                .createdAt(LocalDateTime.now())
                .build();

        when(credentialServices.save(any())).thenReturn(Optional.of(credential));
        when(credentialServices.getAll()).thenReturn(List.of(new Credential(), new Credential()));
        when(credentialServices.getById(anyLong())).thenReturn(Optional.of(credential));
        when(credentialServices.deleteSoft(anyLong())).thenReturn(Optional.of(credential));

        when(credentialMapper.entityToResponseDto(credential)).thenReturn(credentialResponseDto);
        when(credentialMapper.dtoToEntity(any())).thenReturn(credential);
        when(credentialMapper.updateEntityFromDto(anyLong(), any(), any())).thenReturn(credential);
    }

    @Test
    public void test_createCredential() throws Exception {
        mockMvc.perform(post(URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"text\": \"Test text\"}"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.text").value("Test text"));
    }

    @Test
    public void test_update() throws Exception {
        mockMvc.perform(put(URL + "/" + EXIST_ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"text\": \"Test text\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.text").value("Test text"));
    }

    @Test
    public void test_update_notFound() throws Exception {
        when(credentialServices.getById(NOT_EXIST_ID)).thenReturn(Optional.empty());

        mockMvc.perform(put(URL + "/" + NOT_EXIST_ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"text\": \"Test text\"}"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void test_getAll() throws Exception {
        mockMvc.perform(get(URL))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(2));
    }

    @Test
    public void test_getAll_notFound() throws Exception {
        when(credentialServices.getAll()).thenReturn(List.of());

        mockMvc.perform(get(URL))
                .andExpect(status().isNotFound());
    }

    @Test
    public void test_getById() throws Exception {
        mockMvc.perform(get(URL + "/" + EXIST_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.text").value("Test text"));
    }

    @Test
    public void test_getById_notFound() throws Exception {
        when(credentialServices.getById(NOT_EXIST_ID)).thenReturn(Optional.empty());

        mockMvc.perform(get(URL + "/" + NOT_EXIST_ID))
                .andExpect(status().isNotFound());
    }

    @Test
    public void test_delete() throws Exception {
        mockMvc.perform(delete(URL + "/" + EXIST_ID))
                .andExpect(status().isOk());

        verify(credentialServices, times(1)).deleteSoft(EXIST_ID);
    }

    @Test
    public void test_delete_notFound() throws Exception {
        when(credentialServices.deleteSoft(NOT_EXIST_ID)).thenReturn(Optional.empty());

        mockMvc.perform(delete(URL + "/" + NOT_EXIST_ID))
                .andExpect(status().isNotFound());

        verify(credentialServices, times(1)).deleteSoft(NOT_EXIST_ID);
    }
}
