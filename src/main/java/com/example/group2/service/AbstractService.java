package com.example.group2.service;

import com.example.group2.entity.SoftDeleteEntity;
import com.example.group2.repository.AbstractRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Optional;

/**
 * Project: group-2
 *
 * @param <T> тип сущности
 * @param <R> тип репозитория
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 03.10.2023 02:16 |
 * Created with IntelliJ IDEA
 * <p>
 * Абстрактный класс сервисов.
 */
@Transactional
@RequiredArgsConstructor
public abstract class AbstractService<T, R extends AbstractRepository<T>> {

    protected final R repository;

    /**
     * Сохраняет сущность.
     *
     * @param entity сущность для сохранения
     * @return Optional с сохраненной сущностью
     */
    public Optional<T> save(T entity) {
        return Optional.of(repository.save(entity));
    }

    /**
     * Возвращает сущность по указанному идентификатору.
     *
     * @param id идентификатор сущности
     * @return Optional с найденной сущностью или пустой, если сущность не найдена
     */
    public Optional<T> getById(Long id) {
        return repository.findByIdAndIsDeletedFalse(id);
    }

    /**
     * Возвращает список всех сущностей.
     *
     * @return список всех сущностей
     */
    public List<T> getAll() {
        return repository.findAllByIsDeletedFalse();
    }

    /**
     * Удаляет сущность по указанному идентификатору (Soft delete).
     * Если сущность поддерживает мягкое удаление, устанавливает флаг удаления в true.
     *
     * @param id идентификатор сущности
     */
    public Optional<T> deleteSoft(Long id) {
        Optional<T> optionalEntity = repository.findByIdAndIsDeletedFalse(id);
        optionalEntity.ifPresent(entity -> {
            if (entity instanceof SoftDeleteEntity) {
                ((SoftDeleteEntity) entity).setIsDeleted(true);
                repository.save(entity);
            }
        });
        return optionalEntity;
    }
}
