package com.example.group2.dto;

import com.example.group2.entity.Field;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 24.10.2023 10:19 |
 * Created with IntelliJ IDEA
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FieldResponseDto {

    private Long id;

    private String name;

    private String placeholder;

    private Field.Type type;

    private String defaultValue;
}
