package com.example.group2.mapper;

import com.example.group2.dto.FileDto;
import com.example.group2.dto.FileResponseDto;
import com.example.group2.entity.File;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 30.10.2023 17:18 |
 * Created with IntelliJ IDEA
 */
@Component
@RequiredArgsConstructor
public class FileMapperImpl implements FileMapper {

    @Override
    public FileDto multipartToDto(MultipartFile multipartFile) throws IOException {
        if (!multipartFile.isEmpty()) {
            FileDto.FileDtoBuilder dto = FileDto.builder();
            dto.name(multipartFile.getOriginalFilename());
            dto.size(multipartFile.getSize());
            dto.type(multipartFile.getContentType());
            dto.data(multipartFile.getBytes());
            return dto.build();
        }
        return null;
    }

    @Override
    public File dtoToEntity(FileDto dto) {
        if (dto == null) {
            return null;
        }

        File.FileBuilder file = File.builder();

        file.name(dto.getName());
        file.size(dto.getSize());
        file.type(dto.getType());
        byte[] data = dto.getData();
        if (data != null) {
            file.data(Arrays.copyOf(data, data.length));
        }

        return file.build();
    }

    @Override
    public File responseDtoToEntity(FileResponseDto responseDto) {
        if (responseDto == null) {
            return null;
        }

        File.FileBuilder file = File.builder();

        file.id(responseDto.getId());
        file.name(responseDto.getName());
        file.size(responseDto.getSize());
        file.type(responseDto.getType());
        byte[] data = responseDto.getData();
        if (data != null) {
            file.data(Arrays.copyOf(data, data.length));
        }

        return file.build();
    }

    @Override
    public FileResponseDto entityToResponseDto(File entity) {
        if (entity == null) {
            return null;
        }

        FileResponseDto.FileResponseDtoBuilder fileResponseDto = FileResponseDto.builder();

        fileResponseDto.id(entity.getId());
        fileResponseDto.name(entity.getName());
        fileResponseDto.size(entity.getSize());
        fileResponseDto.type(entity.getType());
        byte[] data = entity.getData();
        if (data != null) {
            fileResponseDto.data(Arrays.copyOf(data, data.length));
        }

        return fileResponseDto.build();
    }

    @Override
    public File updateEntityFromDto(Long id, File entity, FileDto dto) {
        if (id == null && dto == null) {
            return entity;
        }

        if (dto != null) {
            entity.setName(dto.getName());
            entity.setSize(dto.getSize());
            entity.setType(dto.getType());
            byte[] data = dto.getData();
            if (data != null) {
                entity.setData(Arrays.copyOf(data, data.length));
            } else {
                entity.setData(null);
            }
        }
        entity.setId(id);

        return entity;
    }
}

