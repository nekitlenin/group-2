package com.example.group2.mapper;

import com.example.group2.dto.CounterpartyDto;
import com.example.group2.dto.CounterpartyResponseDto;
import com.example.group2.entity.Comment;
import com.example.group2.entity.Counterparty;
import com.example.group2.entity.Credential;
import com.example.group2.service.CommentService;
import com.example.group2.service.CredentialService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 26.10.2023 03:57 |
 * Created with IntelliJ IDEA
 */
@Component
@RequiredArgsConstructor
public class CounterpartyMapperImpl implements CounterpartyMapper {

    private final CredentialService credentialService;

    private final CommentService commentService;

    @Override
    public Counterparty dtoToEntity(CounterpartyDto dto) {
        if (dto == null) {
            return null;
        }

        Counterparty.CounterpartyBuilder entity = Counterparty.builder();

        entity.firstName(dto.getFirstName());
        entity.lastName(dto.getLastName());
        entity.middleName(dto.getMiddleName());
        entity.phoneNumber(dto.getPhoneNumber());
        entity.telegramName(dto.getTelegramName());
        entity.email(dto.getEmail());
        entity.country(dto.getCountry());
        if (dto.getCommentIds() != null)
            entity.comments(dto.getCommentIds()
                    .stream()
                    .map(id -> commentService.getById(id).orElse(null))
                    .collect(Collectors.toList()));
        if (dto.getCredentialIds() != null)
            entity.credentials(dto.getCredentialIds()
                    .stream()
                    .map(id -> credentialService.getById(id).orElse(null))
                    .collect(Collectors.toList()));

        return entity.build();
    }

    @Override
    public Counterparty responseDtoToEntity(CounterpartyResponseDto responseDto) {
        if (responseDto == null) {
            return null;
        }

        Counterparty.CounterpartyBuilder entity = Counterparty.builder();

        entity.id(responseDto.getId());
        entity.firstName(responseDto.getFirstName());
        entity.lastName(responseDto.getLastName());
        entity.middleName(responseDto.getMiddleName());
        entity.phoneNumber(responseDto.getPhoneNumber());
        entity.telegramName(responseDto.getTelegramName());
        entity.email(responseDto.getEmail());
        entity.country(responseDto.getCountry());
        if (responseDto.getCommentIds() != null)
            entity.comments(responseDto.getCommentIds()
                    .stream()
                    .map(id -> commentService.getById(id).orElse(null))
                    .collect(Collectors.toList()));
        if (responseDto.getCredentialIds() != null)
            entity.credentials(responseDto.getCredentialIds()
                    .stream()
                    .map(id -> credentialService.getById(id).orElse(null))
                    .collect(Collectors.toList()));

        return entity.build();
    }

    @Override
    public CounterpartyResponseDto entityToResponseDto(Counterparty entity) {
        if (entity == null) {
            return null;
        }

        CounterpartyResponseDto.CounterpartyResponseDtoBuilder responseDto = CounterpartyResponseDto.builder();

        responseDto.id(entity.getId());
        responseDto.firstName(entity.getFirstName());
        responseDto.lastName(entity.getLastName());
        responseDto.middleName(entity.getMiddleName());
        responseDto.phoneNumber(entity.getPhoneNumber());
        responseDto.telegramName(entity.getTelegramName());
        responseDto.email(entity.getEmail());
        responseDto.country(entity.getCountry());
        if (entity.getComments() != null)
            responseDto.commentIds(entity.getComments()
                    .stream()
                    .map(Comment::getId)
                    .collect(Collectors.toList()));
        if (entity.getCredentials() != null)
            responseDto.credentialIds(entity.getCredentials()
                    .stream()
                    .map(Credential::getId)
                    .collect(Collectors.toList()));

        return responseDto.build();
    }

    @Override
    public Counterparty updateEntityFromDto(Long id, Counterparty entity, CounterpartyDto dto) {
        if (id == null && dto == null) {
            return entity;
        }

        if (dto != null) {
            entity.setFirstName(dto.getFirstName());
            entity.setLastName(dto.getLastName());
            entity.setMiddleName(dto.getMiddleName());
            entity.setPhoneNumber(dto.getPhoneNumber());
            entity.setTelegramName(dto.getTelegramName());
            entity.setEmail(dto.getEmail());
            entity.setCountry(dto.getCountry());
            entity.setComments(dto.getCommentIds()
                        .stream()
                        .map(commentId -> commentService.getById(commentId).orElse(null))
                        .collect(Collectors.toList()));
            entity.setCredentials(dto.getCredentialIds()
                        .stream()
                        .map(credentialId -> credentialService.getById(credentialId).orElse(null))
                        .collect(Collectors.toList()));
        }
        entity.setId(id);

        return entity;
    }
}
