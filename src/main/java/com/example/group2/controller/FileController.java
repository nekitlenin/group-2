package com.example.group2.controller;

import com.example.group2.dto.FileDto;
import com.example.group2.dto.FileResponseDto;
import com.example.group2.mapper.FileMapper;
import com.example.group2.service.FileService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 13.10.2023 12:26 |
 * Created with IntelliJ IDEA
 * <p>
 * Контроллер для работы с файлами.
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/files")
public class FileController {

    private final FileService fileService;
    private final FileMapper fileMapper;

    @GetMapping("/download/{id}")
    public ResponseEntity<Resource> download(@PathVariable Long id) {
        log.info("Downloading file with ID: {}", id);
        FileResponseDto fileResponseDto = fileService.getById(id)
                .map(fileMapper::entityToResponseDto)
                .orElse(null);
        if (fileResponseDto != null) {
            Resource resource = new ByteArrayResource(fileResponseDto.getData());
            log.info("File with ID {} downloaded successfully.", id);
            return ResponseEntity.ok()
                    .header(CONTENT_DISPOSITION, "attachment;filename=" + fileResponseDto.getName())
                    .contentType(MediaType.parseMediaType(fileResponseDto.getType()))
                    .body(resource);
        } else {
            log.warn("File with ID {} not found. Download failed.", id);
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/upload")
    public ResponseEntity<FileResponseDto> upload(@RequestParam("file") MultipartFile file) {
        log.info("Uploading a new file: {}", file.isEmpty() ? null : file.getOriginalFilename());
        try {
            FileDto dto = fileMapper.multipartToDto(file);
            FileResponseDto fileResponseDto =
                    fileService.save(fileMapper.dtoToEntity(dto))
                            .map(fileMapper::entityToResponseDto)
                            .orElse(null);

            log.info("New file uploading successfully.");
            return new ResponseEntity<>(fileResponseDto, HttpStatus.OK);
        } catch (Exception e) {
            log.warn("New file is empty! Upload failed.");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Создание нового файла.
     *
     * @param dto данные файла
     * @return созданный файл
     */
    @PostMapping
    public ResponseEntity<FileResponseDto> create(@RequestBody FileDto dto) {
        log.info("Creating a new file: {}", dto);
        FileResponseDto fileResponseDto =
                fileService.save(fileMapper.dtoToEntity(dto))
                        .map(fileMapper::entityToResponseDto)
                        .orElse(null);
        log.info("New file created successfully.");
        return new ResponseEntity<>(fileResponseDto, HttpStatus.CREATED);
    }

    /**
     * Обновление файла.
     *
     * @param id  идентификатор файла
     * @param dto новые данные файла
     * @return обновленный файл
     */
    @PutMapping(value = "/{id}")
    public ResponseEntity<FileResponseDto> update(@PathVariable(name = "id") Long id,
                                                  @RequestBody FileDto dto) {
        log.info("Updating file with ID {}: {}", id, dto);
        FileResponseDto fileResponseDto = fileService.getById(id)
                .map(fileMapper::entityToResponseDto)
                .orElse(null);
        if (fileResponseDto == null) {
            log.warn("File with ID {} not found. Update failed.", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        fileResponseDto = fileService.save(fileMapper.updateEntityFromDto
                        (id, fileMapper.responseDtoToEntity(fileResponseDto), dto))
                .map(fileMapper::entityToResponseDto)
                .orElse(null);
        log.info("File with ID {} updated successfully.", id);
        return new ResponseEntity<>(fileResponseDto, HttpStatus.OK);
    }

    /**
     * Получение всех файлов.
     *
     * @return список файлов
     */
    @GetMapping
    public ResponseEntity<List<FileResponseDto>> getAll() {
        log.info("Get all files");
        List<FileResponseDto> dtos = fileService.getAll().stream()
                .map(fileMapper::entityToResponseDto)
                .collect(Collectors.toList());
        if (!dtos.isEmpty()) {
            log.info("Get {} files.", dtos.size());
            return new ResponseEntity<>(dtos, HttpStatus.OK);
        } else {
            log.warn("No files found.");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Получение файла по идентификатору.
     *
     * @param id идентификатор файла
     * @return найденный файл
     */
    @GetMapping(value = "/{id}")
    public ResponseEntity<FileResponseDto> get(@PathVariable(name = "id") Long id) {
        log.info("Get file with ID {}.", id);
        FileResponseDto dto = fileService.getById(id)
                .map(fileMapper::entityToResponseDto)
                .orElse(null);
        if (dto != null) {
            log.info("Get file with ID {} successfully.", id);
            return new ResponseEntity<>(dto, HttpStatus.OK);
        } else {
            log.warn("File with ID {} not found.", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Удаление файла.
     *
     * @param id идентификатор файла
     * @return удалённый файл
     */
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<FileResponseDto> delete(@PathVariable(name = "id") Long id) {
        log.info("Deleting file with ID {}.", id);
        FileResponseDto dto = fileService.deleteSoft(id)
                .map(fileMapper::entityToResponseDto)
                .orElse(null);
        if (dto != null) {
            log.info("File with ID {} deleted successfully.", id);
            return new ResponseEntity<>(dto, HttpStatus.OK);
        } else {
            log.warn("File with ID {} not found. Deletion failed.", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
