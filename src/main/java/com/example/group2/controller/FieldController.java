package com.example.group2.controller;

import com.example.group2.dto.FieldDto;
import com.example.group2.dto.FieldResponseDto;
import com.example.group2.mapper.FieldMapper;
import com.example.group2.service.FieldService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 13.10.2023 12:26 |
 * Created with IntelliJ IDEA
 * <p>
 * Контроллер для работы с полями.
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/fields")
public class FieldController {

    private final FieldService fieldService;
    private final FieldMapper fieldMapper;

    /**
     * Создание нового поля.
     *
     * @param dto данные поля
     * @return созданное поле
     */
    @PostMapping
    public ResponseEntity<FieldResponseDto> create(@RequestBody FieldDto dto) {
        log.info("Creating a new field: {}", dto);
        FieldResponseDto fieldResponseDto =
                fieldService.save(fieldMapper.dtoToEntity(dto))
                        .map(fieldMapper::entityToResponseDto)
                        .orElse(null);
        log.info("New field created successfully.");
        return new ResponseEntity<>(fieldResponseDto, HttpStatus.CREATED);
    }

    /**
     * Обновление поля.
     *
     * @param id  идентификатор файла
     * @param dto новые данные файла
     * @return обновленное поле
     */
    @PutMapping(value = "/{id}")
    public ResponseEntity<FieldResponseDto> update(@PathVariable(name = "id") Long id,
                                                   @RequestBody FieldDto dto) {
        log.info("Updating field with ID {}: {}", id, dto);
        FieldResponseDto fieldResponseDto = fieldService.getById(id)
                .map(fieldMapper::entityToResponseDto)
                .orElse(null);
        if (fieldResponseDto == null) {
            log.warn("Comment with ID {} not found. Update failed.", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        fieldResponseDto = fieldService.save(fieldMapper.updateEntityFromDto
                        (id, fieldMapper.responseDtoToEntity(fieldResponseDto), dto))
                .map(fieldMapper::entityToResponseDto)
                .orElse(null);
        log.info("Field with ID {} updated successfully.", id);
        return new ResponseEntity<>(fieldResponseDto, HttpStatus.OK);
    }

    /**
     * Получение всех полей.
     *
     * @return список полей
     */
    @GetMapping
    public ResponseEntity<List<FieldResponseDto>> getAll() {
        log.info("Get all fields");
        List<FieldResponseDto> dtos = fieldService.getAll().stream()
                .map(fieldMapper::entityToResponseDto)
                .collect(Collectors.toList());
        if (!dtos.isEmpty()) {
            log.info("Get {} fields.", dtos.size());
            return new ResponseEntity<>(dtos, HttpStatus.OK);
        } else {
            log.warn("No fields found.");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Получение поля по идентификатору.
     *
     * @param id идентификатор поля
     * @return найденное поле
     */
    @GetMapping(value = "/{id}")
    public ResponseEntity<FieldResponseDto> get(@PathVariable(name = "id") Long id) {
        log.info("Get field with ID {}.", id);
        FieldResponseDto dto = fieldService.getById(id)
                .map(fieldMapper::entityToResponseDto)
                .orElse(null);
        if (dto != null) {
            log.info("Get field with ID {} successfully.", id);
            return new ResponseEntity<>(dto, HttpStatus.OK);
        } else {
            log.warn("Field with ID {} not found.", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Удаление поля.
     *
     * @param id идентификатор поля
     * @return удалённое поле
     */
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<FieldResponseDto> delete(@PathVariable(name = "id") Long id) {
        log.info("Deleting field with ID {}.", id);
        FieldResponseDto dto = fieldService.deleteSoft(id)
                .map(fieldMapper::entityToResponseDto)
                .orElse(null);
        if (dto != null) {
            log.info("Field with ID {} deleted successfully.", id);
            return new ResponseEntity<>(dto, HttpStatus.OK);
        } else {
            log.warn("Field with ID {} not found. Deletion failed.", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}