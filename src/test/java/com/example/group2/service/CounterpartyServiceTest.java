package com.example.group2.service;

import com.example.group2.entity.Counterparty;
import com.example.group2.repository.CounterpartyRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 28.09.2023 22:25 |
 * Created with IntelliJ IDEA
 */

@ExtendWith(MockitoExtension.class)
class CounterpartyServiceTest {

    @InjectMocks
    private CounterpartyService counterpartyService;

    @Mock
    private CounterpartyRepository counterpartyRepository;

    @Test
    public void testSaveCounterparty() {
        Counterparty counterpartyToSave = new Counterparty();
        counterpartyToSave.setId(1L);
        counterpartyToSave.setIsDeleted(false);

        Mockito.when(counterpartyRepository.save(any(Counterparty.class))).thenReturn(counterpartyToSave);
        Optional<Counterparty> saveCounterparty = counterpartyService.save(counterpartyToSave);

        saveCounterparty.ifPresent(counterparty -> assertEquals(1L, counterparty.getId()));
    }

    @Test
    public void testGetCounterpartyById() {
        Counterparty counterpartyToFound = new Counterparty();
        counterpartyToFound.setId(1L);
        counterpartyToFound.setFirstName("Test Counterparty");

        Mockito.when(counterpartyRepository.findByIdAndIsDeletedFalse(1L)).thenReturn(Optional.of(counterpartyToFound));
        Optional<Counterparty> foundCounterparty = counterpartyService.getById(1L);

        foundCounterparty.ifPresent(counterparty -> {
            assertEquals(1L, counterparty.getId());
            assertEquals("Test Counterparty", counterparty.getFirstName());
        });
    }

    @Test
    public void testGetAllCounterparties() {
        List<Counterparty> counterparties = new ArrayList<>();
        counterparties.add(new Counterparty());
        counterparties.add(new Counterparty());
        counterparties.add(new Counterparty());

        Mockito.when(counterpartyRepository.findAllByIsDeletedFalse()).thenReturn(counterparties);
        List<Counterparty> allCounterparties = counterpartyService.getAll();

        assertEquals(3, allCounterparties.size());
    }

    @Test
    public void testDeleteCounterparty() {
        Counterparty counterparty = new Counterparty();
        counterparty.setId(1L);
        counterparty.setIsDeleted(false);

        Mockito.when(counterpartyRepository.findByIdAndIsDeletedFalse(1L)).thenReturn(Optional.of(counterparty));
        counterpartyService.deleteSoft(1L);

        assertTrue(counterparty.getIsDeleted());
    }
}