package com.example.group2.entity;

import jakarta.persistence.*;
import lombok.*;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 28.09.2023 22:57 |
 * Created with IntelliJ IDEA
 */
@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "files")
public class File extends SoftDeleteEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "data")
    private byte[] data;

    @Column(name = "size")
    private Long size;

    @Column(name = "type")
    private String type;
}
