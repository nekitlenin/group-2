package com.example.group2.mapper;

import com.example.group2.dto.DocumentDto;
import com.example.group2.dto.DocumentResponseDto;
import com.example.group2.entity.*;
import com.example.group2.service.CounterpartyService;
import com.example.group2.service.FieldService;
import com.example.group2.service.FileService;
import com.example.group2.service.TemplateService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 23.10.2023 05:28 |
 * Created with IntelliJ IDEA
 */
@Component
@RequiredArgsConstructor
public class DocumentMapperImpl implements DocumentMapper {

    private final CounterpartyService counterpartyService;
    private final TemplateService templateService;
    private final FieldService fieldService;
    private final FileService fileService;

    @Override
    public Document dtoToEntity(DocumentDto dto) {
        if (dto == null) {
            return null;
        }

        Document.DocumentBuilder entity = Document.builder();

        entity.name(dto.getName());
        entity.content(dto.getContent());
        entity.type(dto.getType());
        entity.counterparty(counterpartyService.getById(dto.getCounterpartyId()).orElse(null));
        entity.template(templateService.getById(dto.getTemplateId()).orElse(null));
        if (dto.getFieldIds() != null)
            entity.fields(dto.getFieldIds()
                    .stream()
                    .map(field -> fieldService.getById(field).orElse(null))
                    .collect(Collectors.toList()));
        if (dto.getFileIds() != null)
            entity.files(dto.getFileIds()
                    .stream()
                    .map(file -> fileService.getById(file).orElse(null))
                    .collect(Collectors.toList()));
        return entity.build();
    }

    @Override
    public Document responseDtoToEntity(DocumentResponseDto responseDto) {
        if (responseDto == null) {
            return null;
        }

        Document.DocumentBuilder entity = Document.builder();

        entity.id(responseDto.getId());
        entity.name(responseDto.getName());
        entity.content(responseDto.getContent());
        entity.type(responseDto.getType());
        entity.counterparty(counterpartyService.getById(responseDto.getCounterpartyId()).orElse(null));
        entity.template(templateService.getById(responseDto.getTemplateId()).orElse(null));
        if (responseDto.getFieldIds() != null)
            entity.fields(responseDto.getFieldIds()
                    .stream()
                    .map(field -> fieldService.getById(field).orElse(null))
                    .collect(Collectors.toList()));
        if (responseDto.getFileIds() != null)
            entity.files(responseDto.getFileIds()
                    .stream()
                    .map(file -> fileService.getById(file).orElse(null))
                    .collect(Collectors.toList()));
        return entity.build();
    }

    @Override
    public DocumentResponseDto entityToResponseDto(Document entity) {
        if (entity == null)
            return null;
        if (entity.getId() == null)
            return null;

        DocumentResponseDto.DocumentResponseDtoBuilder responseDto = DocumentResponseDto.builder();

        responseDto.id(entity.getId());
        responseDto.templateId(entity.getTemplate() != null ? entity.getTemplate().getId() : null);
        responseDto.counterpartyId(entity.getCounterparty() != null ? entity.getCounterparty().getId() : null);
        responseDto.name(entity.getName());
        responseDto.content(entity.getContent());
        responseDto.type(entity.getType());
        responseDto.createdAt(entity.getCreatedAt());
        if (entity.getFields() != null)
            responseDto.fieldIds(entity.getFields()
                    .stream()
                    .map(Field::getId)
                    .collect(Collectors.toList()));
        if (entity.getFiles() != null)
            responseDto.fileIds(entity.getFiles()
                    .stream()
                    .map(File::getId)
                    .collect(Collectors.toList()));

        return responseDto.build();
    }

    @Override
    public Document updateEntityFromDto(Long id, Document entity, DocumentDto dto) {
        if (id == null && dto == null)
            return entity;

        if (dto != null) {
                entity.setName(dto.getName());
                entity.setContent(dto.getContent());
                entity.setType(dto.getType());
                entity.setCounterparty(counterpartyService.getById(dto.getCounterpartyId()).orElse(null));
                entity.setTemplate(templateService.getById(dto.getTemplateId()).orElse(null));
                entity.setFields(dto.getFieldIds()
                        .stream()
                        .map(field -> fieldService.getById(field).orElse(null))
                        .collect(Collectors.toList()));
                entity.setFiles(dto.getFileIds()
                        .stream()
                        .map(file -> fileService.getById(file).orElse(null))
                        .collect(Collectors.toList()));
        }
        entity.setId(id);
        return entity;
    }
}
