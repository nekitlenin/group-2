package com.example.group2.controller;

import com.example.group2.dto.CommentResponseDto;
import com.example.group2.entity.Comment;
import com.example.group2.mapper.CommentMapper;
import com.example.group2.service.CommentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CommentController.class)
public class CommentControllerTest {
    private static final String URL = "/comments";

    private static final Long EXIST_ID = 1L;

    private static final Long NOT_EXIST_ID = 2L;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CommentService commentService;

    @MockBean
    private CommentMapper commentMapper;

    @BeforeEach
    public void setUp() {
        CommentResponseDto commentResponseDto = CommentResponseDto.builder()
                .text("Test text")
                .build();
        Comment comment = Comment.builder()
                .id(1L)
                .build();

        when(commentService.save(any())).thenReturn(Optional.of(comment));
        when(commentService.getAll()).thenReturn(List.of(new Comment(), new Comment()));
        when(commentService.getById(anyLong())).thenReturn(Optional.of(comment));
        when(commentService.deleteSoft(anyLong())).thenReturn(Optional.of(comment));

        when(commentMapper.entityToResponseDto(comment)).thenReturn(commentResponseDto);
        when(commentMapper.dtoToEntity(any())).thenReturn(comment);
        when(commentMapper.updateEntityFromDto(anyLong(), any(), any())).thenReturn(comment);
    }

    @Test
    public void test_create() throws Exception {
        mockMvc.perform(post(URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"text\": \"Test text\"}"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.text").value("Test text"));
    }

    @Test
    public void test_update() throws Exception {
        mockMvc.perform(put(URL + "/" + EXIST_ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"text\": \"Test text\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.text").value("Test text"));
    }

    @Test
    public void test_update_notFound() throws Exception {
        when(commentService.getById(NOT_EXIST_ID)).thenReturn(Optional.empty());

        mockMvc.perform(put(URL + "/" + NOT_EXIST_ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"text\": \"Updated text\"}"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void test_getAll() throws Exception {
        mockMvc.perform(get(URL))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(2));
    }

    @Test
    public void test_getAll_notFound() throws Exception {
        when(commentService.getAll()).thenReturn(List.of());

        mockMvc.perform(get(URL))
                .andExpect(status().isNotFound());
    }

    @Test
    public void test_getById() throws Exception {
        mockMvc.perform(get(URL + "/" + EXIST_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.text").value("Test text"));
    }

    @Test
    public void test_getById_notFound() throws Exception {
        when(commentService.getById(NOT_EXIST_ID)).thenReturn(Optional.empty());

        mockMvc.perform(get(URL + "/" + NOT_EXIST_ID))
                .andExpect(status().isNotFound());
    }

    @Test
    public void test_delete() throws Exception {
        mockMvc.perform(delete(URL + "/" + EXIST_ID))
                .andExpect(status().isOk());

        verify(commentService, times(1)).deleteSoft(EXIST_ID);
    }

    @Test
    public void test_delete_notFound() throws Exception {
        when(commentService.deleteSoft(NOT_EXIST_ID)).thenReturn(Optional.empty());

        mockMvc.perform(delete(URL + "/" + NOT_EXIST_ID))
                .andExpect(status().isNotFound());

        verify(commentService, times(1)).deleteSoft(NOT_EXIST_ID);
    }
}
