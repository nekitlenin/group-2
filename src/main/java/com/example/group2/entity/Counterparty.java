package com.example.group2.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 22.09.2023 14:45 |
 * Created with IntelliJ IDEA
 */
@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "counterparties")
public class Counterparty extends SoftDeleteEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "middle_name")
    private String middleName;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "telegram_name")
    private String telegramName;

    @Column(name = "email")
    private String email;

    @OneToMany
    @ToString.Exclude
    private List<Credential> credentials;

    @Column(name = "country")
    private String country;

    @OneToMany(mappedBy = "counterparty", cascade = CascadeType.ALL)
    @ToString.Exclude
    private List<Comment> comments;
}
