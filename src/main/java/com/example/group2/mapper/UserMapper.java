package com.example.group2.mapper;

import com.example.group2.dto.UserDto;
import com.example.group2.dto.UserResponseDto;
import com.example.group2.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface UserMapper {

    User dtoToEntity(UserDto dto);

    User responseDtoToEntity(UserResponseDto responseDto);

    UserResponseDto entityToResponseDto(User entity);

    User updateEntityFromDto(Long id, @MappingTarget User entity, UserDto dto);
}
