package com.example.group2.dto;

import com.example.group2.entity.Field;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FieldDto {

    private String name;

    private String placeholder;

    private Field.Type type;

    private String defaultValue;
}
