package com.example.group2.mapper;

import com.example.group2.dto.TemplateDto;
import com.example.group2.dto.TemplateResponseDto;
import com.example.group2.entity.Field;
import com.example.group2.entity.Template;
import com.example.group2.service.FieldService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 24.10.2023 17:31 |
 * Created with IntelliJ IDEA
 */
@Component
@RequiredArgsConstructor
public class TemplateMapperImpl implements TemplateMapper {

    private final FieldService fieldService;

    @Override
    public Template dtoToEntity(TemplateDto dto) {
        if (dto == null) {
            return null;
        }

        Template.TemplateBuilder entity = Template.builder();

        entity.name(dto.getName());
        entity.content(dto.getContent());
        entity.version(dto.getVersion());
        entity.createdAt(dto.getCreatedAt());
        if (dto.getFieldIds() != null)
            entity.fields(dto.getFieldIds()
                    .stream()
                    .map(id -> fieldService.getById(id).orElse(null))
                    .collect(Collectors.toList()));

        return entity.build();
    }

    @Override
    public Template responseDtoToEntity(TemplateResponseDto responseDto) {
        if (responseDto == null) {
            return null;
        }

        Template.TemplateBuilder entity = Template.builder();

        entity.id(responseDto.getId());
        entity.name(responseDto.getName());
        entity.content(responseDto.getContent());
        entity.version(responseDto.getVersion());
        entity.createdAt(responseDto.getCreatedAt());
        if (responseDto.getFieldIds() != null)
            entity.fields(responseDto.getFieldIds()
                    .stream()
                    .map(field -> fieldService.getById(field).orElse(null))
                    .collect(Collectors.toList()));

        return entity.build();
    }

    @Override
    public TemplateResponseDto entityToResponseDto(Template entity) {
        if (entity == null) {
            return null;
        }

        TemplateResponseDto.TemplateResponseDtoBuilder responseDto = TemplateResponseDto.builder();

        responseDto.id(entity.getId());
        responseDto.name(entity.getName());
        responseDto.content(entity.getContent());
        responseDto.version(entity.getVersion());
        responseDto.createdAt(entity.getCreatedAt());
        if (entity.getFields() != null)
            responseDto.fieldIds(entity.getFields()
                    .stream()
                    .map(Field::getId)
                    .collect(Collectors.toList()));
        return responseDto.build();
    }

    @Override
    public Template updateEntityFromDto(Long id, Template entity, TemplateDto dto) {
        if (id == null && dto == null) {
            return entity;
        }

        if (dto != null) {
            entity.setName(dto.getName());
            entity.setContent(dto.getContent());
            entity.setVersion(dto.getVersion());
            entity.setFields(dto.getFieldIds()
                    .stream()
                    .map(field -> fieldService.getById(field).orElse(null))
                    .collect(Collectors.toList()));
        }
        entity.setId(id);

        return entity;
    }
}
