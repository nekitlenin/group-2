package com.example.group2.service;

import com.example.group2.entity.Template;
import com.example.group2.repository.TemplateRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 28.09.2023 22:27 |
 * Created with IntelliJ IDEA
 */
@ExtendWith(MockitoExtension.class)
class TemplateServiceTest {

    @InjectMocks
    private TemplateService templateService;

    @Mock
    private TemplateRepository templateRepository;

    @Test
    public void testSaveDocument() {
        Template templateToSave = new Template();
        templateToSave.setId(1L);
        templateToSave.setIsDeleted(false);

        Mockito.when(templateRepository.save(any(Template.class))).thenReturn(templateToSave);
        Optional<Template> saveTemplate = templateService.save(templateToSave);

        saveTemplate.ifPresent(template -> assertEquals(1L, template.getId()));
    }

    @Test
    public void testGetDocumentById() {
        Template templateToFound = new Template();
        templateToFound.setId(1L);

        Mockito.when(templateRepository.findByIdAndIsDeletedFalse(1L)).thenReturn(Optional.of(templateToFound));
        Optional<Template> foundTemplate = templateService.getById(1L);

        foundTemplate.ifPresent(counterparty -> assertEquals(1L, counterparty.getId()));
    }

    @Test
    public void testGetAllDocuments() {
        List<Template> templates = new ArrayList<>();
        templates.add(new Template());
        templates.add(new Template());
        templates.add(new Template());

        Mockito.when(templateRepository.findAllByIsDeletedFalse()).thenReturn(templates);
        List<Template> allTemplates = templateService.getAll();

        assertEquals(3, allTemplates.size());
    }

    @Test
    public void testDeleteCounterparty() {
        Template template = new Template();
        template.setId(1L);
        template.setIsDeleted(false);

        Mockito.when(templateRepository.findByIdAndIsDeletedFalse(1L)).thenReturn(Optional.of(template));
        templateService.deleteSoft(1L);

        assertTrue(template.getIsDeleted());
    }
}