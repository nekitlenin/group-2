package com.example.group2.mapper;

import com.example.group2.dto.DocumentDto;
import com.example.group2.dto.DocumentResponseDto;
import com.example.group2.entity.Document;
import org.mapstruct.MappingTarget;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 23.10.2023 05:52 |
 * Created with IntelliJ IDEA
 */
public interface DocumentMapper {

    Document dtoToEntity(DocumentDto dto);

    Document responseDtoToEntity(DocumentResponseDto responseDto);

    DocumentResponseDto entityToResponseDto(Document entity);

    Document updateEntityFromDto(Long id, @MappingTarget Document entity, DocumentDto dto);
}
