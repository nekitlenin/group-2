package com.example.group2.mapper;

import com.example.group2.dto.FieldDto;
import com.example.group2.dto.FieldResponseDto;
import com.example.group2.entity.Field;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface FieldMapper {

    Field dtoToEntity(FieldDto dto);

    Field responseDtoToEntity(FieldResponseDto responseDto);

    FieldResponseDto entityToResponseDto(Field entity);

    Field updateEntityFromDto(Long id, @MappingTarget Field entity, FieldDto dto);
}
