package com.example.group2.entity;

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import lombok.Data;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 03.10.2023 02:29 |
 * Created with IntelliJ IDEA
 */
@MappedSuperclass
@Data
public abstract class SoftDeleteEntity {

    @Column(name = "is_deleted")
    private Boolean isDeleted = false;
}
