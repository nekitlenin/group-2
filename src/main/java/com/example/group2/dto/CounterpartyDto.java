package com.example.group2.dto;

import com.example.group2.entity.Comment;
import com.example.group2.entity.Credential;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.OneToMany;
import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CounterpartyDto {

    private String firstName;

    private String lastName;

    private String middleName;

    private String phoneNumber;

    private String telegramName;

    private String email;

    private String country;

    private List<Long> credentialIds;

    private List<Long> commentIds;
}
