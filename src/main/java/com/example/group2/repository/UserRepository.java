package com.example.group2.repository;

import com.example.group2.entity.User;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 22.09.2023 15:13 |
 * Created with IntelliJ IDEA
 */
public interface UserRepository extends AbstractRepository<User> {

}

