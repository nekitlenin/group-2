package com.example.group2.controller;

import com.example.group2.dto.CounterpartyDto;
import com.example.group2.dto.CounterpartyResponseDto;
import com.example.group2.mapper.CounterpartyMapper;
import com.example.group2.service.CounterpartyService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 13.10.2023 12:25 |
 * Created with IntelliJ IDEA
 * <p>
 * Контроллер для работы с контрагентами.
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/counterparties")
public class CounterpartyController {

    private final CounterpartyService counterpartyService;
    private final CounterpartyMapper counterpartyMapper;

    /**
     * Создание нового контрагента.
     *
     * @param dto данные контрагента
     * @return созданный контрагент
     */
    @PostMapping
    public ResponseEntity<CounterpartyResponseDto> create(@RequestBody CounterpartyDto dto) {
        log.info("Creating a new counterparty: {}", dto);
        CounterpartyResponseDto counterpartyResponseDto =
                counterpartyService.save(counterpartyMapper.dtoToEntity(dto))
                        .map(counterpartyMapper::entityToResponseDto)
                        .orElse(null);
        log.info("New counterparty created successfully.");
        return new ResponseEntity<>(counterpartyResponseDto, HttpStatus.CREATED);
    }

    /**
     * Обновление контрагента с указанным идентификатором.
     *
     * @param id  идентификатор контрагента
     * @param dto данные контрагента
     * @return обновленный контрагент
     */
    @PutMapping(value = "/{id}")
    public ResponseEntity<CounterpartyResponseDto> update(@PathVariable(name = "id") Long id,
                                                          @RequestBody CounterpartyDto dto) {
        log.info("Updating counterparty with ID {}: {}", id, dto);
        CounterpartyResponseDto counterpartyResponseDto = counterpartyService.getById(id)
                .map(counterpartyMapper::entityToResponseDto)
                .orElse(null);
        if (counterpartyResponseDto == null) {
            log.warn("Counterparty with ID {} not found. Update failed.", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        counterpartyResponseDto = counterpartyService.save(counterpartyMapper.updateEntityFromDto
                        (id, counterpartyMapper.responseDtoToEntity(counterpartyResponseDto), dto))
                .map(counterpartyMapper::entityToResponseDto)
                .orElse(null);
        log.info("Counterparty with ID {} updated successfully.", id);
        return new ResponseEntity<>(counterpartyResponseDto, HttpStatus.OK);
    }

    /**
     * Получение всех контрагентов.
     *
     * @return список контрагентов
     */
    @GetMapping
    public ResponseEntity<List<CounterpartyResponseDto>> getAll() {
        log.info("Get all counterparties");
        List<CounterpartyResponseDto> dtos = counterpartyService.getAll().stream()
                .map(counterpartyMapper::entityToResponseDto)
                .collect(Collectors.toList());
        if (!dtos.isEmpty()) {
            log.info("Get {} counterparties.", dtos.size());
            return new ResponseEntity<>(dtos, HttpStatus.OK);
        } else {
            log.warn("No counterparties found.");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Получение контрагента по его идентификатору.
     *
     * @param id идентификатор контрагента
     * @return найденный контрагент
     */
    @GetMapping(value = "/{id}")
    public ResponseEntity<CounterpartyResponseDto> get(@PathVariable(name = "id") Long id) {
        log.info("Get counterparty with ID {}.", id);
        CounterpartyResponseDto dto = counterpartyService.getById(id)
                .map(counterpartyMapper::entityToResponseDto)
                .orElse(null);
        if (dto != null) {
            log.info("Get counterparty with ID {} successfully.", id);
            return new ResponseEntity<>(dto, HttpStatus.OK);
        } else {
            log.warn("Counterparty with ID {} not found.", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Удаление контрагента по его идентификатору.
     *
     * @param id идентификатор контрагента
     * @return удалённый контрагент
     */
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<CounterpartyResponseDto> delete(@PathVariable(name = "id") Long id) {
        log.info("Deleting counterparty with ID {}.", id);
        CounterpartyResponseDto dto = counterpartyService.deleteSoft(id)
                .map(counterpartyMapper::entityToResponseDto)
                .orElse(null);
        if (dto != null) {
            log.info("Counterparty with ID {} deleted successfully.", id);
            return new ResponseEntity<>(dto, HttpStatus.OK);
        } else {
            log.warn("Counterparty with ID {} not found. Deletion failed.", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
