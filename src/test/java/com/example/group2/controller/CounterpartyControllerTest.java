package com.example.group2.controller;

import com.example.group2.dto.CounterpartyResponseDto;
import com.example.group2.entity.Counterparty;
import com.example.group2.mapper.CounterpartyMapper;
import com.example.group2.service.CounterpartyService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 17.10.2023 15:52 |
 * Created with IntelliJ IDEA
 */
@WebMvcTest(CounterpartyController.class)
public class CounterpartyControllerTest {

    private static final String URL = "/counterparties";

    private static final Long EXIST_ID = 1L;

    private static final Long NOT_EXIST_ID = 2L;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CounterpartyService counterpartyService;

    @MockBean
    private CounterpartyMapper counterpartyMapper;

    @BeforeEach
    public void setUp() {
        CounterpartyResponseDto counterpartyResponseDto =
                CounterpartyResponseDto.builder()
                .id(1L)
                .firstName("TestDto first name")
                .build();
        Counterparty counterparty = Counterparty.builder()
                .id(1L)
                .firstName("TestEntity first name")
                .build();

        when(counterpartyService.save(any())).thenReturn(Optional.of(counterparty));
        when(counterpartyService.getAll()).thenReturn(List.of(new Counterparty(), new Counterparty()));
        when(counterpartyService.getById(anyLong())).thenReturn(Optional.of(counterparty));
        when(counterpartyService.deleteSoft(anyLong())).thenReturn(Optional.of(counterparty));

        when(counterpartyMapper.entityToResponseDto(any())).thenReturn(counterpartyResponseDto);
        when(counterpartyMapper.dtoToEntity(any())).thenReturn(counterparty);
        when(counterpartyMapper.updateEntityFromDto(anyLong(), any(), any())).thenReturn(counterparty);
    }

    @Test
    public void test_create() throws Exception {
        mockMvc.perform(post(URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"firstName\": \"TestDto first name\"}"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.firstName").value("TestDto first name"));
    }

    @Test
    public void test_update() throws Exception {
        mockMvc.perform(put(URL + "/" + EXIST_ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"firstName\": \"TestDto first name\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(EXIST_ID))
                .andExpect(jsonPath("$.firstName").value("TestDto first name"));
    }

    @Test
    public void test_update_notFound() throws Exception {
        when(counterpartyService.getById(NOT_EXIST_ID)).thenReturn(Optional.empty());

        mockMvc.perform(put(URL + "/" + NOT_EXIST_ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"firstName\": \"Updated text\"}"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void test_getAll() throws Exception {
        mockMvc.perform(get(URL))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(2));
    }

    @Test
    public void test_getAll_notFound() throws Exception {
        when(counterpartyService.getAll()).thenReturn(List.of());

        mockMvc.perform(get(URL))
                .andExpect(status().isNotFound());
    }

    @Test
    public void test_getById() throws Exception {
        mockMvc.perform(get(URL + "/" + EXIST_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value("TestDto first name"));
    }

    @Test
    public void test_getById_notFound() throws Exception {
        when(counterpartyService.getById(NOT_EXIST_ID)).thenReturn(Optional.empty());

        mockMvc.perform(get(URL + "/" + NOT_EXIST_ID))
                .andExpect(status().isNotFound());
    }

    @Test
    public void test_delete() throws Exception {
        mockMvc.perform(delete(URL + "/" + EXIST_ID))
                .andExpect(status().isOk());

        verify(counterpartyService, times(1)).deleteSoft(EXIST_ID);
    }

    @Test
    public void test_delete_notFound() throws Exception {
        when(counterpartyService.deleteSoft(NOT_EXIST_ID)).thenReturn(Optional.empty());

        mockMvc.perform(delete(URL + "/" + NOT_EXIST_ID))
                .andExpect(status().isNotFound());

        verify(counterpartyService, times(1)).deleteSoft(NOT_EXIST_ID);
    }
}
