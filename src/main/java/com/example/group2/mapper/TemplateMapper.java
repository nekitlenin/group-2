package com.example.group2.mapper;

import com.example.group2.dto.TemplateDto;
import com.example.group2.dto.TemplateResponseDto;
import com.example.group2.entity.Template;
import org.mapstruct.MappingTarget;

public interface TemplateMapper {

    Template dtoToEntity(TemplateDto dto);

    Template responseDtoToEntity(TemplateResponseDto responseDto);

    TemplateResponseDto entityToResponseDto(Template entity);

    Template updateEntityFromDto(Long id, @MappingTarget Template entity, TemplateDto dto);
}
