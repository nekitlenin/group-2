package com.example.group2.repository;

import com.example.group2.entity.File;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 28.09.2023 23:08 |
 * Created with IntelliJ IDEA
 */
public interface FileRepository extends AbstractRepository<File> {

}
