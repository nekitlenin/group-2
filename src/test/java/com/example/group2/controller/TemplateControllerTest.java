package com.example.group2.controller;

import com.example.group2.dto.TemplateResponseDto;
import com.example.group2.entity.Template;
import com.example.group2.mapper.TemplateMapper;
import com.example.group2.service.TemplateService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 17.10.2023 15:54 |
 * Created with IntelliJ IDEA
 */
@WebMvcTest(TemplateController.class)
public class TemplateControllerTest {
    private static final String URL = "/templates";

    private static final Long EXIST_ID = 1L;

    private static final Long NOT_EXIST_ID = 2L;



    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TemplateService templateService;

    @MockBean
    private TemplateMapper templateMapper;

    @BeforeEach
    public void setUp() {
        TemplateResponseDto templateResponseDto = TemplateResponseDto.builder()
                .name("Test text")
                .version(111L)
                .createdAt(LocalDateTime.now())
                .build();
        Template template = Template.builder()
                .id(1L)
                .name("Test text")
                .version(111L)
                .createdAt(LocalDateTime.now())
                .build();
        when(templateService.save(any())).thenReturn(Optional.of(template));
        when(templateService.getAll()).thenReturn(List.of(new Template(), new Template()));
        when(templateService.getById(anyLong())).thenReturn(Optional.of(template));
        when(templateService.deleteSoft(anyLong())).thenReturn(Optional.of(template));

        when(templateMapper.entityToResponseDto(template)).thenReturn(templateResponseDto);
        when(templateMapper.dtoToEntity(any())).thenReturn(template);
        when(templateMapper.updateEntityFromDto(anyLong(), any(), any())).thenReturn(template);
    }

    @Test
    public void test_create() throws Exception {
        mockMvc.perform(post(URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"name\": \"Test text\"}"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value("Test text"));
    }

    @Test
    public void test_update() throws Exception {
        mockMvc.perform(put(URL + "/" + EXIST_ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"name\": \"Test text\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Test text"));
    }

    @Test
    public void test_update_notFound() throws Exception {
        when(templateService.getById(NOT_EXIST_ID)).thenReturn(Optional.empty());

        mockMvc.perform(put(URL + "/" + NOT_EXIST_ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"name\": \"Test text\"}"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void test_getAll() throws Exception {
        mockMvc.perform(get(URL))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(2));
    }

    @Test
    public void test_getAll_notFound() throws Exception {
        when(templateService.getAll()).thenReturn(List.of());

        mockMvc.perform(get(URL))
                .andExpect(status().isNotFound());
    }

    @Test
    public void test_getById() throws Exception {
        mockMvc.perform(get(URL + "/" + EXIST_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Test text"));
    }

    @Test
    public void test_getById_notFound() throws Exception {
        when(templateService.getById(NOT_EXIST_ID)).thenReturn(Optional.empty());

        mockMvc.perform(get(URL + "/" + NOT_EXIST_ID))
                .andExpect(status().isNotFound());
    }

    @Test
    public void test_delete() throws Exception {
        mockMvc.perform(delete(URL + "/" + EXIST_ID))
                .andExpect(status().isOk());

        verify(templateService, times(1)).deleteSoft(EXIST_ID);
    }

    @Test
    public void test_delete_notFound() throws Exception {
        when(templateService.deleteSoft(NOT_EXIST_ID)).thenReturn(Optional.empty());

        mockMvc.perform(delete(URL + "/" + NOT_EXIST_ID))
                .andExpect(status().isNotFound());

        verify(templateService, times(1)).deleteSoft(NOT_EXIST_ID);
    }
}
