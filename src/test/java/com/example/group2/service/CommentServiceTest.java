package com.example.group2.service;

import com.example.group2.entity.Comment;
import com.example.group2.repository.CommentRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 28.09.2023 15:16 |
 * Created with IntelliJ IDEA
 */
@ExtendWith(MockitoExtension.class)
class CommentServiceTest {

    @InjectMocks
    private CommentService commentService;

    @Mock
    private CommentRepository commentRepository;

    @Test
    public void testSaveComment() {
        Comment commentToSave = new Comment();
        commentToSave.setId(1L);
        commentToSave.setIsDeleted(false);

        Mockito.when(commentRepository.save(any(Comment.class))).thenReturn(commentToSave);
        Optional<Comment> savedComment = commentService.save(commentToSave);

        savedComment.ifPresent(comment -> {
            assertEquals(1L, comment.getId());
        });
    }

    @Test
    public void testGetCommentById() {
        Comment commentToFound = new Comment();
        commentToFound.setId(1L);
        commentToFound.setText("Test Comment");

        Mockito.when(commentRepository.findByIdAndIsDeletedFalse(1L)).thenReturn(Optional.of(commentToFound));
        Optional<Comment> foundComment = commentService.getById(1L);

        foundComment.ifPresent(comment -> {
            assertEquals(1L, comment.getId());
            assertEquals("Test Comment", comment.getText());
        });
    }

    @Test
    public void testGetAllComments() {
        List<Comment> comments = new ArrayList<>();
        comments.add(new Comment());
        comments.add(new Comment());
        comments.add(new Comment());

        Mockito.when(commentRepository.findAllByIsDeletedFalse()).thenReturn(comments);
        List<Comment> allComments = commentService.getAll();

        assertEquals(3, allComments.size());
    }

    @Test
    public void testDeleteComment() {
        Comment comment = new Comment();
        comment.setId(1L);
        comment.setIsDeleted(false);

        Mockito.when(commentRepository.findByIdAndIsDeletedFalse(1L)).thenReturn(Optional.of(comment));
        commentService.deleteSoft(1L);

        assertTrue(comment.getIsDeleted());
    }
}