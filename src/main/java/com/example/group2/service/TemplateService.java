package com.example.group2.service;

import com.example.group2.entity.Template;
import com.example.group2.repository.TemplateRepository;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 22.09.2023 14:37 |
 * Created with IntelliJ IDEA
 */
@Service
@Transactional
public class TemplateService extends AbstractService<Template, TemplateRepository> {

    private final TemplateRepository templateRepository;

    public TemplateService(TemplateRepository templateRepository) {
        super(templateRepository);
        this.templateRepository = templateRepository;
    }
}
