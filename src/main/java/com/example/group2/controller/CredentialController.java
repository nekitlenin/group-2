package com.example.group2.controller;

import com.example.group2.dto.CredentialDto;
import com.example.group2.dto.CredentialResponseDto;
import com.example.group2.mapper.CredentialMapper;
import com.example.group2.service.CredentialService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 13.10.2023 12:25 |
 * Created with IntelliJ IDEA
 *
 * Контроллер для управления учётными данными.
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/credentials")
public class CredentialController {

    private final CredentialService credentialService;
    private final CredentialMapper credentialMapper;

    /**
     * Создание новой учётной записи.
     *
     * @param dto данные учетной записи
     * @return созданная учетная запись
     */
    @PostMapping
    public ResponseEntity<CredentialResponseDto> create(@RequestBody CredentialDto dto) {
        log.info("Creating a new credential: {}", dto);
        CredentialResponseDto credentialResponseDto =
                credentialService.save(credentialMapper.dtoToEntity(dto))
                        .map(credentialMapper::entityToResponseDto)
                        .orElse(null);
        log.info("New credential created successfully.");
        return new ResponseEntity<>(credentialResponseDto, HttpStatus.CREATED);
    }

    /**
     * Обновление учетной записи с указанным идентификатором.
     *
     * @param id идентификатор учетной записи
     * @param dto обновленные данные учетной записи
     * @return обновленная учетная запись
     */
    @PutMapping(value = "/{id}")
    public ResponseEntity<CredentialResponseDto> update(@PathVariable(name = "id") Long id,
                                                        @RequestBody CredentialDto dto) {
        log.info("Updating credential with ID {}: {}", id, dto);
        CredentialResponseDto credentialResponseDto = credentialService.getById(id)
                .map(credentialMapper::entityToResponseDto)
                .orElse(null);
        if (credentialResponseDto == null) {
            log.warn("Credential with ID {} not found. Update failed.", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        credentialResponseDto = credentialService.save(credentialMapper.updateEntityFromDto
                (id, credentialMapper.responseDtoToEntity(credentialResponseDto), dto))
                .map(credentialMapper::entityToResponseDto)
                .orElse(null);
        log.info("Credential with ID {} updated successfully.", id);
        return new ResponseEntity<>(credentialResponseDto, HttpStatus.OK);
    }

    /**
     * Возвращение всех учетных записей.
     *
     * @return список учетных записей
     */
    @GetMapping
    public ResponseEntity<List<CredentialResponseDto>> getAll() {
        log.info("Get all credential");
        List<CredentialResponseDto> dtos = credentialService.getAll().stream()
                .map(credentialMapper::entityToResponseDto)
                .collect(Collectors.toList());
        if (!dtos.isEmpty()) {
            log.info("Get {} credential.", dtos.size());
            return new ResponseEntity<>(dtos, HttpStatus.OK);
        } else {
            log.warn("No credentials found.");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Получение учетной записи с указанным идентификатором.
     *
     * @param id идентификатор учетной записи
     * @return найденная учетная запись
     */
    @GetMapping(value = "/{id}")
    public ResponseEntity<CredentialResponseDto> get(@PathVariable(name = "id") Long id) {
        log.info("Get credential with ID {}.", id);
        CredentialResponseDto dto = credentialService.getById(id)
                .map(credentialMapper::entityToResponseDto)
                .orElse(null);
        if (dto != null) {
            log.info("Get credential with ID {} successfully.", id);
            return new ResponseEntity<>(dto, HttpStatus.OK);
        } else {
            log.warn("Credential with ID {} not found.", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Удаление учетной записи по его идентификатору.
     *
     * @param id идентификатор учетной записи
     * @return удалённая учетная запись
     */
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<CredentialResponseDto> delete(@PathVariable(name = "id") Long id) {
        log.info("Deleting credential with ID {}.", id);
        CredentialResponseDto dto = credentialService.deleteSoft(id)
                .map(credentialMapper::entityToResponseDto)
                .orElse(null);
        if (dto != null) {
            log.info("Comment with ID {} deleted successfully.", id);
            return new ResponseEntity<>(dto, HttpStatus.OK);
        } else {
            log.warn("Credential with ID {} not found. Deletion failed.", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
