package com.example.group2.repository;

import com.example.group2.entity.Comment;

public interface CommentRepository extends AbstractRepository<Comment> {

}
