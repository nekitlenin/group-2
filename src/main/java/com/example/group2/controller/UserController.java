package com.example.group2.controller;

import com.example.group2.dto.UserDto;
import com.example.group2.dto.UserResponseDto;
import com.example.group2.mapper.UserMapper;
import com.example.group2.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 08.10.2023 14:32 |
 * Created with IntelliJ IDEA
 *
 * Контроллер для работы с пользователями.
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/users")
public class UserController {

    private final UserService userService;
    private final UserMapper userMapper;

    /**
     * Создание нового пользователя.
     *
     * @param dto данные пользователя
     * @return созданный пользователь
     */
    @PostMapping
    public ResponseEntity<UserResponseDto> create(@RequestBody UserDto dto) {
        log.info("Creating a new user: {}", dto);
        UserResponseDto userResponseDto = userService.save(userMapper.dtoToEntity(dto))
                .map(userMapper::entityToResponseDto)
                .orElse(null);
        log.info("New user created successfully.");
        return new ResponseEntity<>(userResponseDto, HttpStatus.CREATED);
    }

    /**
     * Обновление пользователя.
     *
     * @param id идентификатор пользователя
     * @param dto новые данные пользователя
     * @return обновленный пользователь
     */
    @PutMapping(value = "/{id}")
    public ResponseEntity<UserResponseDto> update(@PathVariable(name = "id") Long id,
                                                  @RequestBody UserDto dto) {
        log.info("Updating user with ID {}: {}", id, dto);
        UserResponseDto userResponseDto = userService.getById(id)
                .map(userMapper::entityToResponseDto)
                .orElse(null);
        if (userResponseDto == null) {
            log.warn("Update with ID {} not found. Update failed.", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        userResponseDto = userService.save(userMapper.updateEntityFromDto
                (id, userMapper.responseDtoToEntity(userResponseDto), dto))
                .map(userMapper::entityToResponseDto)
                .orElse(null);
        log.info("User with ID {} updated successfully.", id);
        return new ResponseEntity<>(userResponseDto, HttpStatus.OK);
    }

    /**
     * Получение всех пользователей.
     *
     * @return список пользователей
     */
    @GetMapping
    public ResponseEntity<List<UserResponseDto>> getAll() {
        log.info("Get all users");
        List<UserResponseDto> dtos = userService.getAll().stream()
                .map(userMapper::entityToResponseDto)
                .collect(Collectors.toList());
        if (!dtos.isEmpty()) {
            log.info("Get {} users.", dtos.size());
            return new ResponseEntity<>(dtos, HttpStatus.OK);
        } else {
            log.warn("No users found.");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Получение пользователя по идентификатору.
     *
     * @param id идентификатор пользователя
     * @return найденный пользователь
     */
    @GetMapping(value = "/{id}")
    public ResponseEntity<UserResponseDto> get(@PathVariable(name = "id") Long id) {
        log.info("Get user with ID {}.", id);
        UserResponseDto dto = userService.getById(id)
                .map(userMapper::entityToResponseDto)
                .orElse(null);
        if (dto != null) {
            log.info("Get user with ID {} successfully.", id);
            return new ResponseEntity<>(dto, HttpStatus.OK);
        } else {
            log.warn("User with ID {} not found.", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Удаление пользователя.
     *
     * @param id идентификатор пользователя
     * @return удалённый пользователь
     */
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<UserResponseDto> delete(@PathVariable(name = "id") Long id) {
        log.info("Deleting user with ID {}.", id);
        UserResponseDto dto = userService.deleteSoft(id)
                .map(userMapper::entityToResponseDto)
                .orElse(null);
        if (dto != null) {
            log.info("User with ID {} deleted successfully.", id);
            return new ResponseEntity<>(dto, HttpStatus.OK);
        } else {
            log.warn("User with ID {} not found. Deletion failed.", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
