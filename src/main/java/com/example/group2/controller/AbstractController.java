package com.example.group2.controller;

import com.example.group2.mapper.AbstractMapper;
import com.example.group2.repository.AbstractRepository;
import com.example.group2.service.AbstractService;
import lombok.RequiredArgsConstructor;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 13.10.2023 02:08 |
 * Created with IntelliJ IDEA
 */
@RequiredArgsConstructor
public class AbstractController<T, R extends AbstractRepository<T>, D> {

//    private final AbstractService<T, R> service;
//    private final AbstractMapper<D, T, R> mapper;

//    @PostMapping
//    public ResponseEntity<D> create(@RequestBody D dto) {
//        service.save(mapper.dtoToEntity(dto));
//        return new ResponseEntity<>(dto, HttpStatus.CREATED);
//    }
//
//    @PutMapping(value = "/{id}")
//    public ResponseEntity<D> update(@PathVariable(name = "id") Long id, @RequestBody D dto) {
//        Optional<T> entity = service.getById(id);
//        if (entity.isEmpty())
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//        System.out.println("UPDATE CONTROLLER" + entity.get());
//        T updatedEntity = mapper.updateEntityFromDto(id, entity.get(), dto);
//        System.out.println("UPDATE CONTROLLER" + entity.get() + " " + updatedEntity);
//        service.save(updatedEntity);
//        D updatedDto = mapper.entityToDto(updatedEntity);
//
//        System.out.println(entity.get() + " " + updatedDto);
//        return new ResponseEntity<>(updatedDto, HttpStatus.OK);
//    }
//
//    @GetMapping
//    public ResponseEntity<List<D>> getAll() {
//        List<D> dtos = service.getAll().stream()
//                .map(mapper::entityToDto)
//                .collect(Collectors.toList());
//        return !dtos.isEmpty() ?
//                new ResponseEntity<>(dtos, HttpStatus.OK) :
//                new ResponseEntity<>(HttpStatus.NOT_FOUND);
//    }
//
//    @GetMapping(value = "/{id}")
//    public ResponseEntity<D> get(@PathVariable(name = "id") Long id) {
//        Optional<D> optionalDto = service.getById(id).map(mapper::entityToDto);
//
//        return optionalDto.map(commentDto ->
//                        new ResponseEntity<>(commentDto, HttpStatus.OK))
//                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
//    }
//
//    @DeleteMapping(value = "/{id}")
//    public ResponseEntity<Void> delete(@PathVariable(name = "id") Long id) {
//        service.deleteSoft(id);
//        return new ResponseEntity<>(HttpStatus.OK);
//    }
}
