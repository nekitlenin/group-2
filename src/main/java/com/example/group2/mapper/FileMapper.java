package com.example.group2.mapper;

import com.example.group2.dto.FileDto;
import com.example.group2.dto.FileResponseDto;
import com.example.group2.entity.File;
import org.mapstruct.MappingTarget;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface FileMapper {

    File dtoToEntity(FileDto dto);

    File responseDtoToEntity(FileResponseDto responseDto);

    FileResponseDto entityToResponseDto(File entity);

    FileDto multipartToDto(MultipartFile multipartFile) throws IOException;

    File updateEntityFromDto(Long id, @MappingTarget File entity, FileDto dto);
}
