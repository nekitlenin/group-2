package com.example.group2.service;

import com.example.group2.entity.Field;
import com.example.group2.repository.FieldRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 28.09.2023 22:26 |
 * Created with IntelliJ IDEA
 */
@ExtendWith(MockitoExtension.class)
class FieldServiceTest {

    @InjectMocks
    private FieldService fieldService;

    @Mock
    private FieldRepository fieldRepository;

    @Test
    public void testSaveField() {
        Field fieldToSave = new Field();
        fieldToSave.setId(1L);
        fieldToSave.setIsDeleted(false);

        Mockito.when(fieldRepository.save(any(Field.class))).thenReturn(fieldToSave);
        Optional<Field> saveField = fieldService.save(fieldToSave);

        saveField.ifPresent(field -> assertEquals(1L, field.getId()));
    }

    @Test
    public void testGetFieldById() {
        Field fieldToFound = new Field();
        fieldToFound.setId(1L);
        fieldToFound.setName("Test Found");

        Mockito.when(fieldRepository.findByIdAndIsDeletedFalse(1L)).thenReturn(Optional.of(fieldToFound));
        Optional<Field> foundField = fieldService.getById(1L);

        foundField.ifPresent(counterparty -> {
            assertEquals(1L, counterparty.getId());
            assertEquals("Test Found", counterparty.getName());
        });
    }

    @Test
    public void testGetAllFields() {
        List<Field> fields = new ArrayList<>();
        fields.add(new Field());
        fields.add(new Field());
        fields.add(new Field());

        Mockito.when(fieldRepository.findAllByIsDeletedFalse()).thenReturn(fields);
        List<Field> allFields = fieldService.getAll();

        assertEquals(3, allFields.size());
    }

    @Test
    public void testDeleteField() {
        Field field = new Field();
        field.setId(1L);
        field.setIsDeleted(false);

        Mockito.when(fieldRepository.findByIdAndIsDeletedFalse(1L)).thenReturn(Optional.of(field));
        fieldService.deleteSoft(1L);

        assertTrue(field.getIsDeleted());
    }
}