package com.example.group2.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TemplateDto {

    private String name;

    private String content;

    private Long version;

    private LocalDateTime createdAt;

    private List<Long> fieldIds;
}
