package com.example.group2.entity;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 22.09.2023 13:55 |
 * Created with IntelliJ IDEA
 */
@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "credentials")
public class Credential extends SoftDeleteEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "text")
    private String text;

    @Column(name = "date_create")
    @Setter(AccessLevel.NONE)
    private LocalDateTime createdAt;

    @Column(name = "version")
    private String version;

    @Column(name = "type")
    @Enumerated(EnumType.ORDINAL)
    private Type type;

    @PrePersist
    private void onCreate() {
        this.createdAt = LocalDateTime.now();
    }

    public enum Type {
        PHYSICAL,
        LEGAL
    }
}
