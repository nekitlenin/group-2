package com.example.group2.service;

import com.example.group2.entity.File;
import com.example.group2.repository.FileRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 28.09.2023 23:23 |
 * Created with IntelliJ IDEA
 */
@ExtendWith(MockitoExtension.class)
class FileServiceTest {

    @InjectMocks
    private FileService fileService;

    @Mock
    private FileRepository fileRepository;

    @Test
    public void testSaveFile() {
        File fileToSave = new File();
        fileToSave.setId(1L);
        fileToSave.setIsDeleted(false);

        Mockito.when(fileRepository.save(any(File.class))).thenReturn(fileToSave);
        Optional<File> saveFile = fileService.save(fileToSave);

        saveFile.ifPresent(file -> assertEquals(1L, file.getId()));
    }

    @Test
    public void testGetFileById() {
        File fileToFound = new File();
        fileToFound.setId(1L);
        fileToFound.setName("Test File");

        Mockito.when(fileRepository.findByIdAndIsDeletedFalse(1L)).thenReturn(Optional.of(fileToFound));
        Optional<File> foundFile = fileService.getById(1L);

        foundFile.ifPresent(file -> {
            assertEquals(1L, file.getId());
            assertEquals("Test File", file.getName());
        });
    }

    @Test
    public void testGetAllFiles() {
        List<File> files = new ArrayList<>();
        files.add(new File());
        files.add(new File());
        files.add(new File());

        Mockito.when(fileRepository.findAllByIsDeletedFalse()).thenReturn(files);
        List<File> allFields = fileService.getAll();

        assertEquals(3, allFields.size());
    }

    @Test
    public void testDeleteField() {
        File file = new File();
        file.setId(1L);
        file.setIsDeleted(false);

        Mockito.when(fileRepository.findByIdAndIsDeletedFalse(1L)).thenReturn(Optional.of(file));
        fileService.deleteSoft(1L);

        assertTrue(file.getIsDeleted());
    }
}