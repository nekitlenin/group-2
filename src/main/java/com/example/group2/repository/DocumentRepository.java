package com.example.group2.repository;

import com.example.group2.entity.Document;

public interface DocumentRepository extends AbstractRepository<Document> {

}

