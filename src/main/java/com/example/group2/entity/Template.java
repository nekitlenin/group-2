package com.example.group2.entity;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 24.09.2023 15:23 |
 * Created with IntelliJ IDEA
 */
@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "templates")
public class Template extends SoftDeleteEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "content")
    private String content;

    @Column(name = "version")
    private Long version;

    @Column(name = "date_create")
    @Setter(AccessLevel.NONE)
    private LocalDateTime createdAt;

    @OneToMany
    @ToString.Exclude
    private List<Field> fields;

    @PrePersist
    private void onCreate() {
        this.version = 1L;
        this.createdAt = LocalDateTime.now();
    }

    @PreUpdate
    private void onUpdate() {
        this.version++;
    }
}
