package com.example.group2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;
import java.util.Optional;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 03.10.2023 02:10 |
 * Created with IntelliJ IDEA
 */
@NoRepositoryBean
public interface AbstractRepository<E> extends JpaRepository<E, Long> {

    List<E> findAllByIsDeletedFalse();

    Optional<E> findByIdAndIsDeletedFalse(Long id);
}
