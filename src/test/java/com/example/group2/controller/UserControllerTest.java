package com.example.group2.controller;

import com.example.group2.dto.UserResponseDto;
import com.example.group2.entity.User;
import com.example.group2.mapper.UserMapper;
import com.example.group2.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 17.10.2023 15:55 |
 * Created with IntelliJ IDEA
 */
@WebMvcTest(UserController.class)
public class UserControllerTest {

    private static final String URL = "/users";

    private static final Long EXIST_ID = 1L;

    private static final Long NOT_EXIST_ID = 2L;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @MockBean
    private UserMapper userMapper;

    @BeforeEach
    public void setUp() {
        UserResponseDto userResponseDto = UserResponseDto.builder()
                .firstName("Test text")
                .build();
        User user = User.builder()
                .firstName("Test text")
                .build();
        when(userService.save(any())).thenReturn(Optional.of(user));
        when(userService.getAll()).thenReturn(List.of(new User(), new User()));
        when(userService.getById(anyLong())).thenReturn(Optional.of(user));
        when(userService.deleteSoft(anyLong())).thenReturn(Optional.of(user));

        when(userMapper.entityToResponseDto(user)).thenReturn(userResponseDto);
        when(userMapper.dtoToEntity(any())).thenReturn(user);
        when(userMapper.updateEntityFromDto(anyLong(), any(), any())).thenReturn(user);
    }

    @Test
    public void test_create() throws Exception {
        mockMvc.perform(post(URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"firstName\": \"Test text\"}"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.firstName").value("Test text"));
    }

    @Test
    public void test_update() throws Exception {
        mockMvc.perform(put(URL + "/" + EXIST_ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"firstName\": \"Test text\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value("Test text"));
    }

    @Test
    public void test_update_notFound() throws Exception {
        when(userService.getById(NOT_EXIST_ID)).thenReturn(Optional.empty());

        mockMvc.perform(put(URL + "/" + NOT_EXIST_ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"firstName\": \"Test text\"}"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void test_getAll() throws Exception {
        mockMvc.perform(get(URL))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(2));
    }

    @Test
    public void test_getAll_notFound() throws Exception {
        when(userService.getAll()).thenReturn(List.of());

        mockMvc.perform(get(URL))
                .andExpect(status().isNotFound());
    }

    @Test
    public void test_getById() throws Exception {
        mockMvc.perform(get(URL + "/" + EXIST_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value("Test text"));
    }

    @Test
    public void test_getById_notFound() throws Exception {
        when(userService.getById(NOT_EXIST_ID)).thenReturn(Optional.empty());

        mockMvc.perform(get(URL + "/" + NOT_EXIST_ID))
                .andExpect(status().isNotFound());
    }

    @Test
    public void test_delete() throws Exception {
        mockMvc.perform(delete(URL + "/" + EXIST_ID))
                .andExpect(status().isOk());

        verify(userService, times(1)).deleteSoft(EXIST_ID);
    }

    @Test
    public void test_delete_notFound() throws Exception {
        when(userService.deleteSoft(NOT_EXIST_ID)).thenReturn(Optional.empty());

        mockMvc.perform(delete(URL + "/" + NOT_EXIST_ID))
                .andExpect(status().isNotFound());

        verify(userService, times(1)).deleteSoft(NOT_EXIST_ID);
    }
}
