package com.example.group2.mapper;

import com.example.group2.dto.CounterpartyDto;
import com.example.group2.dto.CounterpartyResponseDto;
import com.example.group2.entity.Counterparty;
import org.mapstruct.MappingTarget;

public interface CounterpartyMapper {
    Counterparty dtoToEntity(CounterpartyDto dto);

    Counterparty responseDtoToEntity(CounterpartyResponseDto responseDto);

    CounterpartyResponseDto entityToResponseDto(Counterparty entity);

    Counterparty updateEntityFromDto(Long id, @MappingTarget Counterparty entity, CounterpartyDto dto);
}
