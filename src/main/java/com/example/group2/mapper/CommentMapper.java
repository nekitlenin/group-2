package com.example.group2.mapper;

import com.example.group2.dto.CommentDto;
import com.example.group2.dto.CommentResponseDto;
import com.example.group2.entity.Comment;
import org.mapstruct.MappingTarget;

public interface CommentMapper {

    Comment dtoToEntity(CommentDto dto);

    Comment responseDtoToEntity(CommentResponseDto responseDto);

    CommentResponseDto entityToResponseDto(Comment entity);

    Comment updateEntityFromDto(Long id, @MappingTarget Comment entity, CommentDto dto);
}
