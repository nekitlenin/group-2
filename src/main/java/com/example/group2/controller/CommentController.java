package com.example.group2.controller;

import com.example.group2.dto.CommentDto;
import com.example.group2.dto.CommentResponseDto;
import com.example.group2.mapper.CommentMapper;
import com.example.group2.service.CommentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 13.10.2023 01:15 |
 * Created with IntelliJ IDEA
 *
 *
 * Контроллер для работы с комментариями.
 *
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/comments")
public class CommentController {

    private final CommentService commentService;
    private final CommentMapper commentMapper;

    /**
     * Создание нового комментария.
     *
     * @param dto данные комментария
     * @return созданный комментарий
     */
    @PostMapping
    public ResponseEntity<CommentResponseDto> create(@RequestBody CommentDto dto) {
        log.info("Creating a new comment: {}", dto);
        CommentResponseDto commentResponseDto =
                commentService.save(commentMapper.dtoToEntity(dto))
                        .map(commentMapper::entityToResponseDto)
                        .orElse(null);
        log.info("New comment created successfully.");
        return new ResponseEntity<>(commentResponseDto, HttpStatus.CREATED);
    }

    /**
     * Обновление комментария с указанным идентификатором.
     *
     * @param id идентификатор комментария
     * @param dto данные комментария
     * @return обновленный комментарий
     */
    @PutMapping(value = "/{id}")
    public ResponseEntity<CommentResponseDto> update(@PathVariable(name = "id") Long id,
                                                     @RequestBody CommentDto dto) {
        log.info("Updating comment with ID {}: {}", id, dto);
        CommentResponseDto commentResponseDto = commentService.getById(id)
                .map(commentMapper::entityToResponseDto)
                .orElse(null);
        if (commentResponseDto == null) {
            log.warn("Comment with ID {} not found. Update failed.", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        commentResponseDto = commentService.save(commentMapper.updateEntityFromDto
                (id, commentMapper.responseDtoToEntity(commentResponseDto), dto))
                .map(commentMapper::entityToResponseDto)
                .orElse(null);
        log.info("Comment with ID {} updated successfully.", id);
        return new ResponseEntity<>(commentResponseDto, HttpStatus.OK);
    }

    /**
     * Получение всех комментариев.
     *
     * @return список всех комментариев
     */
    @GetMapping
    public ResponseEntity<List<CommentResponseDto>> getAll() {
        log.info("Get all comments");
        List<CommentResponseDto> dtos = commentService.getAll().stream()
                .map(commentMapper::entityToResponseDto)
                .collect(Collectors.toList());
        if (!dtos.isEmpty()) {
            log.info("Get {} comments.", dtos.size());
            return new ResponseEntity<>(dtos, HttpStatus.OK);
        } else {
            log.warn("No comments found.");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Получение комментария по его идентификатору.
     *
     * @param id идентификатор комментария
     * @return найденный комментарий
     */
    @GetMapping(value = "/{id}")
    public ResponseEntity<CommentResponseDto> get(@PathVariable(name = "id") Long id) {
        log.info("Get comment with ID {}.", id);
        CommentResponseDto dto = commentService.getById(id)
                .map(commentMapper::entityToResponseDto)
                .orElse(null);
        if (dto != null) {
            log.info("Get comment with ID {} successfully.", id);
            return new ResponseEntity<>(dto, HttpStatus.OK);
        } else {
            log.warn("Comment with ID {} not found.", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Удаление комментария по его идентификатору.
     *
     * @param id идентификатор комментария
     * @return удалённый комментарий
     */
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<CommentResponseDto> delete(@PathVariable(name = "id") Long id) {
        log.info("Deleting comment with ID {}.", id);
        CommentResponseDto optionalDto = commentService.deleteSoft(id)
                .map(commentMapper::entityToResponseDto)
                .orElse(null);
        if (optionalDto != null) {
            log.info("Comment with ID {} deleted successfully.", id);
            return new ResponseEntity<>(optionalDto, HttpStatus.OK);
        } else {
            log.warn("Comment with ID {} not found. Deletion failed.", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
