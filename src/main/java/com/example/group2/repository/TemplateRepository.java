package com.example.group2.repository;

import com.example.group2.entity.Template;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 22.09.2023 14:34 |
 * Created with IntelliJ IDEA
 */
public interface TemplateRepository extends AbstractRepository<Template> {

}

