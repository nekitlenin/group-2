package com.example.group2.service;

import com.example.group2.entity.Document;
import com.example.group2.repository.DocumentRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 28.09.2023 22:26 |
 * Created with IntelliJ IDEA
 */

@ExtendWith(MockitoExtension.class)
class DocumentServiceTest {

    @InjectMocks
    private DocumentService documentService;

    @Mock
    private DocumentRepository documentRepository;

    @Test
    public void testSaveDocument() {
        Document documentToSave = new Document();
        documentToSave.setId(1L);
        documentToSave.setIsDeleted(false);

        Mockito.when(documentRepository.save(any(Document.class))).thenReturn(documentToSave);
        Optional<Document> saveDocument = documentService.save(documentToSave);

        saveDocument.ifPresent(document -> assertEquals(1L, document.getId()));
    }

    @Test
    public void testGetDocumentById() {
        Document documentToFound = new Document();
        documentToFound.setId(1L);
        documentToFound.setName("Test Document");

        Mockito.when(documentRepository.findByIdAndIsDeletedFalse(1L)).thenReturn(Optional.of(documentToFound));
        Optional<Document> foundDocument = documentService.getById(1L);

        foundDocument.ifPresent(counterparty -> {
            assertEquals(1L, counterparty.getId());
            assertEquals("Test Document", counterparty.getName());
        });
    }

    @Test
    public void testGetAllDocuments() {
        List<Document> documents = new ArrayList<>();
        documents.add(new Document());
        documents.add(new Document());
        documents.add(new Document());

        Mockito.when(documentRepository.findAllByIsDeletedFalse()).thenReturn(documents);
        List<Document> allDocuments = documentService.getAll();

        assertEquals(3, allDocuments.size());
    }

    @Test
    public void testDeleteCounterparty() {
        Document document = new Document();
        document.setId(1L);
        document.setIsDeleted(false);

        Mockito.when(documentRepository.findByIdAndIsDeletedFalse(1L)).thenReturn(Optional.of(document));
        documentService.deleteSoft(1L);

        assertTrue(document.getIsDeleted());
    }
}