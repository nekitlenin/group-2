package com.example.group2.controller;

import com.example.group2.dto.DocumentDto;
import com.example.group2.dto.DocumentResponseDto;
import com.example.group2.mapper.DocumentMapper;
import com.example.group2.service.DocumentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 13.10.2023 12:25 |
 * Created with IntelliJ IDEA
 * <p>
 * Контроллер для работы с документами.
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/documents")
public class DocumentController {

    private final DocumentService documentService;
    private final DocumentMapper documentMapper;

    /**
     * Обновление существующего документа на основе связанного шаблона.
     *
     * @param documentId идентификатор документа
     * @return обновлённый документ по связанному шаблону
     */
    @PutMapping("{documentId}/template")
    public ResponseEntity<DocumentResponseDto> updateExistingDocumentFromTemplate(
            @PathVariable(name = "documentId") Long documentId) {
        log.info("Updating document with Id: {} from linked template.", documentId);
        DocumentResponseDto dto = documentService.getById(documentId)
                .map(documentMapper::entityToResponseDto)
                .orElse(null);
        if (dto == null) {
            log.warn("Document with Id {} not found. Update failed.", documentId);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else if (dto.getTemplateId() == null) {
            log.warn("Document with Id {} found, but linked template is not found. Update failed.", documentId);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        DocumentResponseDto updatedDto = documentMapper.entityToResponseDto(documentService.createFromTemplate
                (dto.getTemplateId(), documentMapper.responseDtoToEntity(dto)).orElse(null));
        log.info("Document with Id {} updated from linked template successfully.", documentId);
        return new ResponseEntity<>(updatedDto, HttpStatus.OK);
    }

    /**
     * Обновление существующего документа на основе заданного шаблона.
     *
     * @param documentId идентификатор документа
     * @param templateId идентификатор шаблона
     * @return обновлённый документ по заданному шаблону
     */
    @PutMapping("/{documentId}/template/{templateId}")
    public ResponseEntity<DocumentResponseDto> updateExistingDocumentFromTemplate(
            @PathVariable(name = "templateId") Long templateId,
            @PathVariable(name = "documentId") Long documentId) {
        log.info("Updating document with Id: {} from template with Id: {}", documentId, templateId);
        DocumentResponseDto dto = documentService.getById(documentId)
                .map(documentMapper::entityToResponseDto)
                .orElse(null);
        if (dto == null) {
            log.warn("Document with Id {} not found. Update failed.", documentId);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        DocumentResponseDto updatedDto = documentMapper.entityToResponseDto(documentService.createFromTemplate
                (templateId, documentMapper.responseDtoToEntity(dto)).orElse(null));
        if (updatedDto == null) {
            log.warn("Template with Id {} not found. Update failed.", templateId);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        log.info("Document with Id {} updated from template with Id {} successfully.", documentId, templateId);
        return new ResponseEntity<>(updatedDto, HttpStatus.OK);
    }

    /**
     * Создание нового документа на основе заданного шаблона.
     *
     * @param templateId идентификатор шаблона
     * @return созданный документ по заданному шаблону
     */
    @PostMapping("/template/{templateId}")
    public ResponseEntity<DocumentResponseDto> createNewDocumentFromTemplate(
            @PathVariable(name = "templateId") Long templateId,
            @RequestBody DocumentDto dto) {
        log.info("Creating a new document from template with Id: {}, with data: {}", templateId, dto);
        DocumentResponseDto createdDocument = documentMapper.entityToResponseDto
                (documentService.createFromTemplate(templateId, documentMapper.dtoToEntity(dto)).orElse(null));
        if (createdDocument == null) {
            log.warn("Template with ID {} not found. Update failed.", templateId);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        log.info("New document created from template successfully.");
        return new ResponseEntity<>(createdDocument, HttpStatus.CREATED);
    }

    /**
     * Создание нового документа с нуля или по шаблону (если указан идентификатор шаблона, то вызывается
     * метод createNewDocumentFromTemplate(templateId, dto))
     *
     * @param dto данные нового документа
     * @return созданный документ
     */
    @PostMapping
    public ResponseEntity<DocumentResponseDto> create(@RequestBody DocumentDto dto) {
        log.info("Creating a new document: {}", dto);
        if (dto.getTemplateId() != null)
            return createNewDocumentFromTemplate(dto.getTemplateId(), dto);
        DocumentResponseDto documentResponseDto = documentService.save(documentMapper.dtoToEntity(dto))
                .map(documentMapper::entityToResponseDto)
                .orElse(null);
        log.info("New document created successfully.");
        return new ResponseEntity<>(documentResponseDto, HttpStatus.CREATED);
    }

    /**
     * Обновление документа по указанному идентификатору.
     *
     * @param id  идентификатор документа
     * @param dto новые данные документа
     * @return обновленный документ
     */
    @PutMapping(value = "/{id}")
    public ResponseEntity<DocumentResponseDto> update(@PathVariable(name = "id") Long id,
                                                      @RequestBody DocumentDto dto) {
        log.info("Updating document with ID {}: {}", id, dto);
        DocumentResponseDto documentResponseDto = documentService.getById(id)
                .map(documentMapper::entityToResponseDto)
                .orElse(null);
        if (documentResponseDto == null) {
            log.warn("Document with ID {} not found. Update failed.", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        documentResponseDto = documentService.save(documentMapper.updateEntityFromDto
                        (id, documentMapper.responseDtoToEntity(documentResponseDto), dto))
                .map(documentMapper::entityToResponseDto)
                .orElse(null);
        log.info("Document with ID {} updated successfully.", id);
        return new ResponseEntity<>(documentResponseDto, HttpStatus.OK);
    }

    /**
     * Получение всех документов.
     *
     * @return список всех документов
     */
    @GetMapping
    public ResponseEntity<List<DocumentResponseDto>> getAll() {
        log.info("Get all documents");
        List<DocumentResponseDto> dtos = documentService.getAll().stream()
                .map(documentMapper::entityToResponseDto)
                .collect(Collectors.toList());
        if (!dtos.isEmpty()) {
            log.info("Get {} documents.", dtos.size());
            return new ResponseEntity<>(dtos, HttpStatus.OK);
        } else {
            log.warn("No documents found.");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Получение документа по его ID.
     *
     * @param id идентификатор документа
     * @return найденный документ
     */
    @GetMapping(value = "/{id}")
    public ResponseEntity<DocumentResponseDto> get(@PathVariable(name = "id") Long id) {
        log.info("Get document with ID {}.", id);
        DocumentResponseDto dto = documentService.getById(id)
                .map(documentMapper::entityToResponseDto)
                .orElse(null);
        if (dto != null) {
            log.info("Get document with ID {} successfully.", id);
            return new ResponseEntity<>(dto, HttpStatus.OK);
        } else {
            log.warn("Document with ID {} not found.", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Удаление документа по его идентификатору.
     *
     * @param id идентификатор документа
     * @return удалённый документ
     */
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<DocumentResponseDto> delete(@PathVariable(name = "id") Long id) {
        log.info("Deleting document with ID {}.", id);
        DocumentResponseDto dto = documentService.deleteSoft(id)
                .map(documentMapper::entityToResponseDto)
                .orElse(null);
        if (dto != null) {
            log.info("Document with ID {} deleted successfully.", id);
            return new ResponseEntity<>(dto, HttpStatus.OK);
        } else {
            log.warn("Document with ID {} not found. Deletion failed.", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
