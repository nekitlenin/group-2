package com.example.group2.service;

import com.example.group2.entity.User;
import com.example.group2.repository.UserRepository;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 22.09.2023 15:15 |
 * Created with IntelliJ IDEA
 */
@Service
@Transactional
public class UserService extends AbstractService<User, UserRepository> {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }
}
