-- liquibase formatted sql
-- changeset nekitlenin@gmail.com:001_create_table

create table counterparties
(
    id            bigserial
        constraint counterparties_pkey
            primary key,
    is_deleted    boolean,
    country       varchar(255),
    email         varchar(255),
    first_name    varchar(255),
    last_name     varchar(255),
    middle_name   varchar(255),
    phone_number  varchar(255),
    telegram_name varchar(255)
);

create table comments
(
    id              bigserial
        constraint comments_pkey
            primary key,
    is_deleted      boolean,
    date_create     timestamp(6),
    text            varchar(255),
    counterparty_id bigint
        constraint fk4l32dlpudw7k3mk2hjqyvrsr3
            references counterparties
);

create table credentials
(
    id          bigserial
        constraint credentials_pkey
            primary key,
    is_deleted  boolean,
    date_create timestamp(6),
    text        varchar(255),
    type        smallint
        constraint credentials_type_check
            check ((type >= 0) AND (type <= 1)),
    version     varchar(255)
);

create table fields
(
    id           bigserial
        constraint fields_pkey
            primary key,
    is_deleted   boolean,
    defaultvalue varchar(255),
    name         varchar(255),
    placeholder  varchar(255),
    type         smallint
        constraint fields_type_check
            check ((type >= 0) AND (type <= 2))
);

create table templates
(
    id          bigserial
        constraint templates_pkey
            primary key,
    is_deleted  boolean,
    date_create timestamp(6),
    name        varchar(255),
    content     varchar(255),
    version     bigint
);

create table files
(
    id         bigserial
        constraint files_pkey
            primary key,
    is_deleted boolean,
    data       bytea,
    size       bigint,
    type       varchar(255),
    name       varchar(255)
);

create table users
(
    id            bigserial
        constraint users_pkey
            primary key,
    is_deleted    boolean,
    date_create   timestamp(6),
    email         varchar(255),
    first_name    varchar(255),
    last_name     varchar(255),
    last_visit    timestamp(6),
    middle_name   varchar(255),
    password      varchar(255),
    telegram_name varchar(255),
    date_update   timestamp(6)
);

create table documents
(
    id              bigserial
        constraint documents_pkey
            primary key,
    is_deleted      boolean,
    date_create     timestamp(6),
    name            varchar(255),
    content         varchar(255),
    type            smallint
        constraint fields_type_check
            check ((type >= 0) AND (type <= 4)),
    counterparty_id bigint
        constraint fkonsif9psuuyhis0i95r5qyxys
            references counterparties,
    template_id     bigint
        constraint fk854a9xn57ofqd0df68lajm0og
            references templates
);

create table counterparties_credentials
(
    counterparty_id bigint not null
        constraint fkmhi1eo62eykh5vsg9nyimu882
            references counterparties,
    credentials_id  bigint not null
        constraint uk_30n9fo5faohco4q1vflxcdnn2
            unique
        constraint fkkx4rn6xjg9oakvjjeh7wv66rc
            references credentials
);

create table documents_fields
(
    document_id bigint not null
        constraint fkp5qhw6gr07l1whs6qbwb9ha66
            references documents,
    fields_id   bigint not null
        constraint uk_n252vsvc47vqrgbe76rw89lc4
            unique
        constraint fk5b27tx7uya9sf8s08qbmcn2m3
            references fields
);

create table documents_files
(
    document_id bigint not null
        constraint fkawv03iyd2ddmqd1thvm1pm061
            references documents,
    files_id    bigint not null
        constraint fkftfuivg53488q685bmmy3h1r1
            references files
);

create table templates_fields
(
    template_id bigint not null
        constraint fkac7n2n2je6v8irevncp0qeh9y
            references templates,
    fields_id   bigint not null
        constraint uk_18u8ai0i3afcbd1b0ouchceit
            unique
        constraint fk685a33fpar58irsgi5xqa7ohy
            references fields
);
