package com.example.group2.mapper;

import com.example.group2.dto.CommentDto;
import com.example.group2.dto.CommentResponseDto;
import com.example.group2.entity.Comment;
import com.example.group2.service.CounterpartyService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 24.10.2023 17:14 |
 * Created with IntelliJ IDEA
 */
@Component
@RequiredArgsConstructor
public class CommentMapperImpl implements CommentMapper {

    private final CounterpartyService counterpartyService;

    @Override
    public Comment dtoToEntity(CommentDto dto) {
        if (dto == null) {
            return null;
        }

        Comment.CommentBuilder entity = Comment.builder();

        entity.text(dto.getText());
        entity.createdAt(dto.getCreatedAt());
        entity.counterparty(counterpartyService.getById(dto.getCounterpartyId()).orElse(null));

        return entity.build();
    }

    @Override
    public Comment responseDtoToEntity(CommentResponseDto responseDto) {
        if (responseDto == null) {
            return null;
        }

        Comment.CommentBuilder entity = Comment.builder();

        entity.id(responseDto.getId());
        entity.text(responseDto.getText());
        entity.createdAt(responseDto.getCreatedAt());
        entity.counterparty(counterpartyService.getById(responseDto.getId()).orElse(null));

        return entity.build();
    }

    @Override
    public CommentResponseDto entityToResponseDto(Comment entity) {
        if (entity == null) {
            return null;
        }

        CommentResponseDto.CommentResponseDtoBuilder responseDto = CommentResponseDto.builder();

        responseDto.id(entity.getId());
        responseDto.text(entity.getText());
        responseDto.createdAt(entity.getCreatedAt());
        responseDto.counterpartyId(entity.getCounterparty() != null ? entity.getCounterparty().getId() : null);

        return responseDto.build();
    }

    @Override
    public Comment updateEntityFromDto(Long id, Comment entity, CommentDto dto) {
        if (id == null && dto == null) {
            return entity;
        }

        if (dto != null) {
            entity.setText(dto.getText());
            entity.setCounterparty(counterpartyService.getById(dto.getCounterpartyId()).orElse(null));
        }
        entity.setId(id);

        return entity;
    }
}
