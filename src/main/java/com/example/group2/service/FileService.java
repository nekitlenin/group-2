package com.example.group2.service;

import com.example.group2.entity.File;
import com.example.group2.repository.FileRepository;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

/**
 * Project: group-2
 *
 * @author nikita
 * Email:  nekitlenin@gmail.com |
 * Create: 28.09.2023 23:10 |
 * Created with IntelliJ IDEA
 */
@Service
@Transactional
public class FileService extends AbstractService<File, FileRepository> {

    private final FileRepository fileRepository;

    public FileService(FileRepository fileRepository) {
        super(fileRepository);
        this.fileRepository = fileRepository;
    }
}
